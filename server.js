import connection from "./components/connection.js";
import React from "react";
import ReactDOM from "react-dom";

import ArrowScoreForm from "./components/ArrowScoreForm";
import DartBattleForm from "./components/DartBattleForm";
import Reset from "./components/Reset";
import UndoButton from "./components/UndoButton";

import { playerPool } from "./config";

const top_score = 301;

class DartServer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			settings: null,
			prevStates: [],
			arrows: {},
			victory: { active: false, player: null },
			playerPool: playerPool.map((name, idx) => ({ name, id: idx }))
		};

		this.db = props.conn.database();
		this.tempScore = 0;
		this.song = null;
		this.voice = new p5.Speech("Google UK English Male");
		this.handleUndo = this.handleUndo.bind(this);
		this.handleNewBattle = this.handleNewBattle.bind(this);
		this.handleArrowScore = this.handleArrowScore.bind(this);
		this.handleReset = this.handleReset.bind(this);
	}

	isLegFinished(score, thrown) {
		return this.calculateScore(score, thrown) == 0;
	}

	componentDidMount() {
		// set listener on firebase database to control state
		this.db.ref("battle/settings").on("value", snapshot => {
			if (snapshot) {
				let newSettings = { ...snapshot.val() };
				this.setState({ settings: newSettings });
			}
		});
		this.db.ref("battle/arrows").on("value", snapshot => {
			const arrows = snapshot.val() !== null ? snapshot.val() : {};
			this.setState({ arrows });
		});
	}

	getCurrentPlayer() {
		return this.state.settings.isPlayerAsTurn ? "playerA" : "playerB";
	}

	calculateScore(current, thrown) {
		return parseInt(current) - parseInt(thrown);
	}

	addArrowScore(score) {
		let prevStates = [...this.state.prevStates];
		const currentSettings = { ...this.state.settings };
		prevStates.push(currentSettings);
		this.setState({ prevStates: prevStates });

		// clean up before continuing
		this.removePreviousArrows();
		this.db
			.ref("battle/arrows")
			.push({ score, player: this.getCurrentPlayer() });
	}

	removePreviousArrows() {
		const lastKey = Object.keys(this.state.arrows).pop();
		const lastArrow = this.state.arrows[lastKey];

		if (lastArrow && lastArrow.player != this.getCurrentPlayer())
			this.db.ref("battle/arrows").remove();
	}

	switchPlayerTurn(state) {
		console.log("switching turn");
		state.settings.isPlayerAsTurn = !state.settings.isPlayerAsTurn;

		this.tempScore = 0;
		this.db.ref("battle/settings").set(state.settings);
	}

	currentPlayerIsThrowing(state) {
		return (
			Object.keys(this.state.arrows).filter(
				arrow =>
					this.state.arrows[arrow].player == this.getCurrentPlayer()
			).length < 2
		);
	}

	speak(score) {
		const currentPlayer = this.getCurrentPlayer();
		switch (score) {
			// 	case "dead":
			// 		this.voice.setRate(0.9)
			// 		this.voice.setPitch(2)
			// 		this.voice.speak("Whoops")
			// 		this.voice.setRate(0.5)
			// 		this.voice.setPitch(1)
			// 		this.voice.speak("You are dead")
			// 		break;
			// case "winner":
			// 		this.voice.setRate(0.9)
			// 		this.voice.setPitch(1)
			// 		this.voice.speak("Congratulations, " + this.state.settings[currentPlayer].name)
			// 		this.song = new Audio("./app/sounds/finish.mp3");
			// 		this.song.play();
			// 		break;
			// case "leg":
			// 		this.voice.setRate(0.9)
			// 		this.voice.setPitch(1)
			// 		this.voice.speak("And the leg goes to, " + this.state.settings[currentPlayer].name)
			// 		break;
			// case 0:
			// 		this.voice.setRate(0.9)
			// 		this.voice.setPitch(1)
			// 		this.voice.speak("Good luck with your next set of doubles!")
			// 		break;
			// case 26:
			// 		this.voice.setRate(0.75)
			// 		this.voice.speak("It's the classic, 26!")
			// 		break
			default:
				this.voice.setRate(0.9);
				this.voice.speak(score);
				break;
		}
	}

	handleArrowScore(arrowScore) {
		let state = { ...this.state };
		const currentPlayer = this.getCurrentPlayer();
		const currentScore = state.settings[currentPlayer].score;
		if (!state.arrows) state.arrows = {};
		this.tempScore = parseInt(this.tempScore) + parseInt(arrowScore);
		const legIsFinished = this.isLegFinished(currentScore, this.tempScore);

		if (legIsFinished) {
			this.addArrowScore(arrowScore);
			setTimeout(() => this.setupNewLeg(currentPlayer), 1500);
			return;
		}

		// player throws dead
		if (this.calculateScore(currentScore, this.tempScore) < 2) {
			this.addArrowScore(arrowScore);
			this.speak("dead");
			setTimeout(() => this.switchPlayerTurn(state), 1500);
			return;
		}

		if (this.currentPlayerIsThrowing(state)) {
			this.addArrowScore(arrowScore);

			// player has finished turn by throwing three arrows
		} else {
			state.settings[currentPlayer].score = this.calculateScore(
				currentScore,
				this.tempScore
			);
			this.addArrowScore(arrowScore);
			this.speak(this.tempScore);
			setTimeout(() => this.switchPlayerTurn(state), 1500);
		}
	}

	handleReset(scores) {
		this.tempScore = 0;
		this.db
			.ref()
			.child("/battle/settings/playerA")
			.update({ score: scores.scoreA });
		this.db
			.ref()
			.child("/battle/settings/playerB")
			.update({ score: scores.scoreB });
	}

	setupNewLeg(winningPlayer) {
		let settings = { ...this.state.settings };
		settings[winningPlayer].legs = settings[winningPlayer].legs + 1;

		if (settings[winningPlayer].legs > settings.legs / 2) {
			const losingPlayer =
				winningPlayer == "playerA" ? "playerB" : "playerA";

			const formdata = new FormData();
			formdata.append("loser", JSON.stringify(settings[losingPlayer]));
			formdata.append("winner", JSON.stringify(settings[winningPlayer]));

			console.log(settings[winningPlayer]);

			this.speak("winner");
			this.db
				.ref("battle/victory")
				.set({ active: true, player: settings[winningPlayer].name });
			this.db.ref("battle/arrows").remove();
			this.tempScore = 0;
			this.setState({ prevStates: [] });
		} else {
			this.speak("leg");
			settings.playerA.score = top_score;
			settings.playerB.score = top_score;
			settings.legStarter =
				settings.legStarter == "playerA" ? "playerB" : "playerA";
			settings.isPlayerAsTurn = settings.legStarter == "playerA";
			this.tempScore = 0;
			this.db.ref("battle").set({
				settings: settings,
				arrows: null,
				victory: { active: false, player: "" }
			});
		}
	}

	handleNewBattle(data) {
		console.log(data);
		console.log(this.state.playerPool);
		const playerDataA = this.state.playerPool.find(
			player => player.id == data.playerA
		);
		const playerDataB = this.state.playerPool.find(
			player => player.id == data.playerB
		);

		const settings = {
			playerA: {
				name: playerDataA.name,
				id: playerDataA.id,
				legs: 0,
				score: top_score
			},
			playerB: {
				name: playerDataB.name,
				id: playerDataB.id,
				legs: 0,
				score: top_score
			},
			isPlayerAsTurn: true,
			legs: data.legs,
			type: data.type,
			legStarter: "playerA"
		};
		this.db.ref("battle").set({
			settings,
			arrows: null,
			victory: { active: false, player: "" }
		});
		this.setState({ prevStates: [] });
	}

	handleUndo(e) {
		e.preventDefault();
		if (this.state.arrows && Object.keys(this.state.arrows).length > 0) {
			// recalculate temporary score
			const lastKey = Object.keys(this.state.arrows).pop();
			this.tempScore = this.state.arrows[lastKey]
				? parseInt(this.tempScore) -
				  parseInt(this.state.arrows[lastKey].score)
				: 0;

			// remove last arrow
			let arrows = { ...this.state.arrows };
			delete arrows[lastKey];
			this.db.ref("battle/arrows").set(arrows);
		}

		if (
			this.state.prevStates &&
			this.state.prevStates[this.state.prevStates.length - 1]
		) {
			let prevStates = [...this.state.prevStates];
			const newState = prevStates.pop();
			this.db.ref("battle/settings").set(newState);
			this.setState({ prevStates: prevStates });
		} else {
			console.log("No earlier state present");
		}
	}

	render() {
		const totalArrows = this.state.arrows
			? Object.keys(this.state.arrows).filter(
					arrow =>
						this.state.arrows[arrow].player ==
						this.getCurrentPlayer()
			  ).length
			: 0;

		return (
			<div className="server">
				<header>
					<div className="logo-wrapper">
						<img src="./app/images/logo.png" />
					</div>
					<div className="title-wrapper">
						<h1>FX Open - Control Tower</h1>
					</div>
				</header>
				<DartBattleForm
					playerPool={this.state.playerPool}
					handleSubmit={this.handleNewBattle}
				/>
				<ArrowScoreForm
					handleSubmit={this.handleArrowScore}
					counter={totalArrows + 1}
				/>
				<Reset handleSubmit={this.handleReset} />
				<UndoButton handleClick={this.handleUndo} />
				<div className="kill-song">
					<h1>Kill the song</h1>
					<button
						type="submit"
						onClick={e => {
							e.preventDefault();
							this.song.pause();
						}}
					>
						Kill song
					</button>
				</div>
			</div>
		);
	}
}

const fb = connection.fb();

ReactDOM.render(<DartServer conn={fb} />, document.getElementById("server"));
