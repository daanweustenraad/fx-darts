import React from "react";

const TOP_SCORE = 301;

export default class DartBattleForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			playerA: "",
			playerB: "",
			type: "First round",
			legs: 3
		};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(field, e) {
		let state = { ...this.state };
		state[field] = e.target.value;
		this.setState(state);
	}

	handleSubmit(e) {
		e.preventDefault();

		this.props.handleSubmit(this.state);
		this.setState({
			type: "First round",
			legs: 3
		});
	}

	render() {
		const playerPool = this.props.playerPool;

		return (
			<form id="battleform" onSubmit={this.handleSubmit}>
				<h1>New battle</h1>
				<div className="field-block">
					<label>Player #1</label>
					<select onChange={this.handleChange.bind(this, "playerA")}>
						<option value={0}></option>
						{playerPool.map(player => (
							<option value={player.id} key={player.id}>
								{player.name}
							</option>
						))}
					</select>
				</div>
				<div className="field-block">
					<label>Player #2</label>
					<select onChange={this.handleChange.bind(this, "playerB")}>
						<option value={0}></option>
						{playerPool.map(player => (
							<option value={player.id} key={player.id}>
								{player.name}
							</option>
						))}
					</select>
				</div>
				<div className="field-block">
					<label>Phase</label>
					<select
						onChange={this.handleChange.bind(this, "type")}
						value={this.state.type}
					>
						<option value="First round">First round</option>
						<option value="Quarter final">Quarter final</option>
						<option value="Semi final">Semi final</option>
						<option value="Finale">Finale</option>
					</select>
				</div>
				<div className="field-block">
					<label>Legs</label>
					<select
						onChange={this.handleChange.bind(this, "legs")}
						value={this.state.legs}
					>
						<option value="3">3</option>
						<option value="5">5</option>
						<option value="7">7</option>
					</select>
					<br />
					<button type="submit">Start new battle!</button>
				</div>
			</form>
		);
	}
}
