import React from 'react'

export default class Reset extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            scoreA: "",
            scoreB: ""
        }
    }

    handleChange(key, e) {
        this.setState({ [key]: e.target.value })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.handleSubmit(this.state)
        this.setState({ scoreA: "", scoreB: "" })
    }

    render() {
        return (
            <div className="Reset">
                <h1>Master Reset</h1>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="field-block">
                        <label>Score Player #1</label>
                        <input value={this.state.scoreA} type="number" onChange={this.handleChange.bind(this, "scoreA")} />
                    </div>
                    <div className="field-block">
                        <label>Score Player #2</label>
                        <input value={this.state.scoreB} type="number" onChange={this.handleChange.bind(this, "scoreB")} />
                    </div>
                    <button type="submit">Reset the scores</button>
                </form>
            </div>
        )
    }
}
