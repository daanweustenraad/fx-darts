import React from 'react'

export default function UndoButton (props) {
  return (
    <div className="UndoButton">
      <h1>Appletje Z</h1>
      <button id="appeltje-z" onClick={props.handleClick}>
        <img src="app/images/command.png" width="40" height="40" />
        <span>Z</span>
      </button>
    </div>
  )
}
