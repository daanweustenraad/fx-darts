import React from "react";

export default class ArrowScoreForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { arrowScore: "" };

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		if (!this.state.arrowScore) return false;

		this.props.handleSubmit(Math.abs(this.state.arrowScore));
		this.setState({ arrowScore: "" });
		this.nameInput.focus();
	}

	handleChange(e) {
		this.setState({ arrowScore: e.target.value });
	}

	render() {
		return (
			<div className="ScoreForm">
				<h1>Score</h1>
				<form id="dartform" onSubmit={this.handleSubmit}>
					<div className="field-block">
						<label id="arrowcount">
							Arrow #
							<span className="counter">
								{Math.min(this.props.counter, 3)}
							</span>
						</label>
						<input
							type="number"
							onChange={this.handleChange}
							ref={input => {
								this.nameInput = input;
							}}
							value={this.state.arrowScore}
						/>
					</div>
					<button type="submit">Add Arrow-score</button>
				</form>
			</div>
		);
	}
}
