import * as firebase from 'firebase'

// Initialize Firebase
class Connection {
  constructor() {
    const config = {
      apiKey: "AIzaSyDMvo0boWvNjBwIujSyJrhgfJ3FobE0Kq4",
      authDomain: "fx-darts.firebaseapp.com",
      databaseURL: "https://fx-darts.firebaseio.com",
      projectId: "fx-darts",
      storageBucket: "",
      messagingSenderId: "458575952910"
    };
    firebase.initializeApp(config);
    this.conn = firebase
  }

  fb() {
    return this.conn
  }
}

export default new Connection
