webpackJsonp([1],{

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _connection = __webpack_require__(2);

	var _connection2 = _interopRequireDefault(_connection);

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(48);

	var _reactDom2 = _interopRequireDefault(_reactDom);

	var _ArrowScoreForm = __webpack_require__(59);

	var _ArrowScoreForm2 = _interopRequireDefault(_ArrowScoreForm);

	var _DartBattleForm = __webpack_require__(60);

	var _DartBattleForm2 = _interopRequireDefault(_DartBattleForm);

	var _Reset = __webpack_require__(61);

	var _Reset2 = _interopRequireDefault(_Reset);

	var _UndoButton = __webpack_require__(62);

	var _UndoButton2 = _interopRequireDefault(_UndoButton);

	var _config = __webpack_require__(63);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var top_score = 301;

	var DartServer = function (_React$Component) {
		_inherits(DartServer, _React$Component);

		function DartServer(props) {
			_classCallCheck(this, DartServer);

			var _this = _possibleConstructorReturn(this, (DartServer.__proto__ || Object.getPrototypeOf(DartServer)).call(this, props));

			_this.state = {
				settings: null,
				prevStates: [],
				arrows: {},
				victory: { active: false, player: null },
				playerPool: _config.playerPool.map(function (name, idx) {
					return { name: name, id: idx };
				})
			};

			_this.db = props.conn.database();
			_this.tempScore = 0;
			_this.song = null;
			_this.voice = new p5.Speech("Google UK English Male");
			_this.handleUndo = _this.handleUndo.bind(_this);
			_this.handleNewBattle = _this.handleNewBattle.bind(_this);
			_this.handleArrowScore = _this.handleArrowScore.bind(_this);
			_this.handleReset = _this.handleReset.bind(_this);
			return _this;
		}

		_createClass(DartServer, [{
			key: "isLegFinished",
			value: function isLegFinished(score, thrown) {
				return this.calculateScore(score, thrown) == 0;
			}
		}, {
			key: "componentDidMount",
			value: function componentDidMount() {
				var _this2 = this;

				// set listener on firebase database to control state
				this.db.ref("battle/settings").on("value", function (snapshot) {
					if (snapshot) {
						var newSettings = _extends({}, snapshot.val());
						_this2.setState({ settings: newSettings });
					}
				});
				this.db.ref("battle/arrows").on("value", function (snapshot) {
					var arrows = snapshot.val() !== null ? snapshot.val() : {};
					_this2.setState({ arrows: arrows });
				});
			}
		}, {
			key: "getCurrentPlayer",
			value: function getCurrentPlayer() {
				return this.state.settings.isPlayerAsTurn ? "playerA" : "playerB";
			}
		}, {
			key: "calculateScore",
			value: function calculateScore(current, thrown) {
				return parseInt(current) - parseInt(thrown);
			}
		}, {
			key: "addArrowScore",
			value: function addArrowScore(score) {
				var prevStates = [].concat(_toConsumableArray(this.state.prevStates));
				var currentSettings = _extends({}, this.state.settings);
				prevStates.push(currentSettings);
				this.setState({ prevStates: prevStates });

				// clean up before continuing
				this.removePreviousArrows();
				this.db.ref("battle/arrows").push({ score: score, player: this.getCurrentPlayer() });
			}
		}, {
			key: "removePreviousArrows",
			value: function removePreviousArrows() {
				var lastKey = Object.keys(this.state.arrows).pop();
				var lastArrow = this.state.arrows[lastKey];

				if (lastArrow && lastArrow.player != this.getCurrentPlayer()) this.db.ref("battle/arrows").remove();
			}
		}, {
			key: "switchPlayerTurn",
			value: function switchPlayerTurn(state) {
				console.log("switching turn");
				state.settings.isPlayerAsTurn = !state.settings.isPlayerAsTurn;

				this.tempScore = 0;
				this.db.ref("battle/settings").set(state.settings);
			}
		}, {
			key: "currentPlayerIsThrowing",
			value: function currentPlayerIsThrowing(state) {
				var _this3 = this;

				return Object.keys(this.state.arrows).filter(function (arrow) {
					return _this3.state.arrows[arrow].player == _this3.getCurrentPlayer();
				}).length < 2;
			}
		}, {
			key: "speak",
			value: function speak(score) {
				var currentPlayer = this.getCurrentPlayer();
				switch (score) {
					// 	case "dead":
					// 		this.voice.setRate(0.9)
					// 		this.voice.setPitch(2)
					// 		this.voice.speak("Whoops")
					// 		this.voice.setRate(0.5)
					// 		this.voice.setPitch(1)
					// 		this.voice.speak("You are dead")
					// 		break;
					// case "winner":
					// 		this.voice.setRate(0.9)
					// 		this.voice.setPitch(1)
					// 		this.voice.speak("Congratulations, " + this.state.settings[currentPlayer].name)
					// 		this.song = new Audio("./app/sounds/finish.mp3");
					// 		this.song.play();
					// 		break;
					// case "leg":
					// 		this.voice.setRate(0.9)
					// 		this.voice.setPitch(1)
					// 		this.voice.speak("And the leg goes to, " + this.state.settings[currentPlayer].name)
					// 		break;
					// case 0:
					// 		this.voice.setRate(0.9)
					// 		this.voice.setPitch(1)
					// 		this.voice.speak("Good luck with your next set of doubles!")
					// 		break;
					// case 26:
					// 		this.voice.setRate(0.75)
					// 		this.voice.speak("It's the classic, 26!")
					// 		break
					default:
						this.voice.setRate(0.9);
						this.voice.speak(score);
						break;
				}
			}
		}, {
			key: "handleArrowScore",
			value: function handleArrowScore(arrowScore) {
				var _this4 = this;

				var state = _extends({}, this.state);
				var currentPlayer = this.getCurrentPlayer();
				var currentScore = state.settings[currentPlayer].score;
				if (!state.arrows) state.arrows = {};
				this.tempScore = parseInt(this.tempScore) + parseInt(arrowScore);
				var legIsFinished = this.isLegFinished(currentScore, this.tempScore);

				if (legIsFinished) {
					this.addArrowScore(arrowScore);
					setTimeout(function () {
						return _this4.setupNewLeg(currentPlayer);
					}, 1500);
					return;
				}

				// player throws dead
				if (this.calculateScore(currentScore, this.tempScore) < 2) {
					this.addArrowScore(arrowScore);
					this.speak("dead");
					setTimeout(function () {
						return _this4.switchPlayerTurn(state);
					}, 1500);
					return;
				}

				if (this.currentPlayerIsThrowing(state)) {
					this.addArrowScore(arrowScore);

					// player has finished turn by throwing three arrows
				} else {
					state.settings[currentPlayer].score = this.calculateScore(currentScore, this.tempScore);
					this.addArrowScore(arrowScore);
					this.speak(this.tempScore);
					setTimeout(function () {
						return _this4.switchPlayerTurn(state);
					}, 1500);
				}
			}
		}, {
			key: "handleReset",
			value: function handleReset(scores) {
				this.tempScore = 0;
				this.db.ref().child("/battle/settings/playerA").update({ score: scores.scoreA });
				this.db.ref().child("/battle/settings/playerB").update({ score: scores.scoreB });
			}
		}, {
			key: "setupNewLeg",
			value: function setupNewLeg(winningPlayer) {
				var settings = _extends({}, this.state.settings);
				settings[winningPlayer].legs = settings[winningPlayer].legs + 1;

				if (settings[winningPlayer].legs > settings.legs / 2) {
					var losingPlayer = winningPlayer == "playerA" ? "playerB" : "playerA";

					var formdata = new FormData();
					formdata.append("loser", JSON.stringify(settings[losingPlayer]));
					formdata.append("winner", JSON.stringify(settings[winningPlayer]));

					console.log(settings[winningPlayer]);

					this.speak("winner");
					this.db.ref("battle/victory").set({ active: true, player: settings[winningPlayer].name });
					this.db.ref("battle/arrows").remove();
					this.tempScore = 0;
					this.setState({ prevStates: [] });
				} else {
					this.speak("leg");
					settings.playerA.score = top_score;
					settings.playerB.score = top_score;
					settings.legStarter = settings.legStarter == "playerA" ? "playerB" : "playerA";
					settings.isPlayerAsTurn = settings.legStarter == "playerA";
					this.tempScore = 0;
					this.db.ref("battle").set({
						settings: settings,
						arrows: null,
						victory: { active: false, player: "" }
					});
				}
			}
		}, {
			key: "handleNewBattle",
			value: function handleNewBattle(data) {
				console.log(data);
				console.log(this.state.playerPool);
				var playerDataA = this.state.playerPool.find(function (player) {
					return player.id == data.playerA;
				});
				var playerDataB = this.state.playerPool.find(function (player) {
					return player.id == data.playerB;
				});

				var settings = {
					playerA: {
						name: playerDataA.name,
						id: playerDataA.id,
						legs: 0,
						score: top_score
					},
					playerB: {
						name: playerDataB.name,
						id: playerDataB.id,
						legs: 0,
						score: top_score
					},
					isPlayerAsTurn: true,
					legs: data.legs,
					type: data.type,
					legStarter: "playerA"
				};
				this.db.ref("battle").set({
					settings: settings,
					arrows: null,
					victory: { active: false, player: "" }
				});
				this.setState({ prevStates: [] });
			}
		}, {
			key: "handleUndo",
			value: function handleUndo(e) {
				e.preventDefault();
				if (this.state.arrows && Object.keys(this.state.arrows).length > 0) {
					// recalculate temporary score
					var lastKey = Object.keys(this.state.arrows).pop();
					this.tempScore = this.state.arrows[lastKey] ? parseInt(this.tempScore) - parseInt(this.state.arrows[lastKey].score) : 0;

					// remove last arrow
					var arrows = _extends({}, this.state.arrows);
					delete arrows[lastKey];
					this.db.ref("battle/arrows").set(arrows);
				}

				if (this.state.prevStates && this.state.prevStates[this.state.prevStates.length - 1]) {
					var prevStates = [].concat(_toConsumableArray(this.state.prevStates));
					var newState = prevStates.pop();
					this.db.ref("battle/settings").set(newState);
					this.setState({ prevStates: prevStates });
				} else {
					console.log("No earlier state present");
				}
			}
		}, {
			key: "render",
			value: function render() {
				var _this5 = this;

				var totalArrows = this.state.arrows ? Object.keys(this.state.arrows).filter(function (arrow) {
					return _this5.state.arrows[arrow].player == _this5.getCurrentPlayer();
				}).length : 0;

				return _react2.default.createElement(
					"div",
					{ className: "server" },
					_react2.default.createElement(
						"header",
						null,
						_react2.default.createElement(
							"div",
							{ className: "logo-wrapper" },
							_react2.default.createElement("img", { src: "./app/images/logo.png" })
						),
						_react2.default.createElement(
							"div",
							{ className: "title-wrapper" },
							_react2.default.createElement(
								"h1",
								null,
								"FX Open - Control Tower"
							)
						)
					),
					_react2.default.createElement(_DartBattleForm2.default, {
						playerPool: this.state.playerPool,
						handleSubmit: this.handleNewBattle
					}),
					_react2.default.createElement(_ArrowScoreForm2.default, {
						handleSubmit: this.handleArrowScore,
						counter: totalArrows + 1
					}),
					_react2.default.createElement(_Reset2.default, { handleSubmit: this.handleReset }),
					_react2.default.createElement(_UndoButton2.default, { handleClick: this.handleUndo }),
					_react2.default.createElement(
						"div",
						{ className: "kill-song" },
						_react2.default.createElement(
							"h1",
							null,
							"Kill the song"
						),
						_react2.default.createElement(
							"button",
							{
								type: "submit",
								onClick: function onClick(e) {
									e.preventDefault();
									_this5.song.pause();
								}
							},
							"Kill song"
						)
					)
				);
			}
		}]);

		return DartServer;
	}(_react2.default.Component);

	var fb = _connection2.default.fb();

	_reactDom2.default.render(_react2.default.createElement(DartServer, { conn: fb }), document.getElementById("server"));

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var ArrowScoreForm = function (_React$Component) {
		_inherits(ArrowScoreForm, _React$Component);

		function ArrowScoreForm(props) {
			_classCallCheck(this, ArrowScoreForm);

			var _this = _possibleConstructorReturn(this, (ArrowScoreForm.__proto__ || Object.getPrototypeOf(ArrowScoreForm)).call(this, props));

			_this.state = { arrowScore: "" };

			_this.handleChange = _this.handleChange.bind(_this);
			_this.handleSubmit = _this.handleSubmit.bind(_this);
			return _this;
		}

		_createClass(ArrowScoreForm, [{
			key: "handleSubmit",
			value: function handleSubmit(e) {
				e.preventDefault();
				if (!this.state.arrowScore) return false;

				this.props.handleSubmit(Math.abs(this.state.arrowScore));
				this.setState({ arrowScore: "" });
				this.nameInput.focus();
			}
		}, {
			key: "handleChange",
			value: function handleChange(e) {
				this.setState({ arrowScore: e.target.value });
			}
		}, {
			key: "render",
			value: function render() {
				var _this2 = this;

				return _react2.default.createElement(
					"div",
					{ className: "ScoreForm" },
					_react2.default.createElement(
						"h1",
						null,
						"Score"
					),
					_react2.default.createElement(
						"form",
						{ id: "dartform", onSubmit: this.handleSubmit },
						_react2.default.createElement(
							"div",
							{ className: "field-block" },
							_react2.default.createElement(
								"label",
								{ id: "arrowcount" },
								"Arrow #",
								_react2.default.createElement(
									"span",
									{ className: "counter" },
									Math.min(this.props.counter, 3)
								)
							),
							_react2.default.createElement("input", {
								type: "number",
								onChange: this.handleChange,
								ref: function ref(input) {
									_this2.nameInput = input;
								},
								value: this.state.arrowScore
							})
						),
						_react2.default.createElement(
							"button",
							{ type: "submit" },
							"Add Arrow-score"
						)
					)
				);
			}
		}]);

		return ArrowScoreForm;
	}(_react2.default.Component);

	exports.default = ArrowScoreForm;

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var TOP_SCORE = 301;

	var DartBattleForm = function (_React$Component) {
		_inherits(DartBattleForm, _React$Component);

		function DartBattleForm(props) {
			_classCallCheck(this, DartBattleForm);

			var _this = _possibleConstructorReturn(this, (DartBattleForm.__proto__ || Object.getPrototypeOf(DartBattleForm)).call(this, props));

			_this.state = {
				playerA: "",
				playerB: "",
				type: "First round",
				legs: 3
			};

			_this.handleSubmit = _this.handleSubmit.bind(_this);
			return _this;
		}

		_createClass(DartBattleForm, [{
			key: "handleChange",
			value: function handleChange(field, e) {
				var state = _extends({}, this.state);
				state[field] = e.target.value;
				this.setState(state);
			}
		}, {
			key: "handleSubmit",
			value: function handleSubmit(e) {
				e.preventDefault();

				this.props.handleSubmit(this.state);
				this.setState({
					type: "First round",
					legs: 3
				});
			}
		}, {
			key: "render",
			value: function render() {
				var playerPool = this.props.playerPool;

				return _react2.default.createElement(
					"form",
					{ id: "battleform", onSubmit: this.handleSubmit },
					_react2.default.createElement(
						"h1",
						null,
						"New battle"
					),
					_react2.default.createElement(
						"div",
						{ className: "field-block" },
						_react2.default.createElement(
							"label",
							null,
							"Player #1"
						),
						_react2.default.createElement(
							"select",
							{ onChange: this.handleChange.bind(this, "playerA") },
							_react2.default.createElement("option", { value: 0 }),
							playerPool.map(function (player) {
								return _react2.default.createElement(
									"option",
									{ value: player.id, key: player.id },
									player.name
								);
							})
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "field-block" },
						_react2.default.createElement(
							"label",
							null,
							"Player #2"
						),
						_react2.default.createElement(
							"select",
							{ onChange: this.handleChange.bind(this, "playerB") },
							_react2.default.createElement("option", { value: 0 }),
							playerPool.map(function (player) {
								return _react2.default.createElement(
									"option",
									{ value: player.id, key: player.id },
									player.name
								);
							})
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "field-block" },
						_react2.default.createElement(
							"label",
							null,
							"Phase"
						),
						_react2.default.createElement(
							"select",
							{
								onChange: this.handleChange.bind(this, "type"),
								value: this.state.type
							},
							_react2.default.createElement(
								"option",
								{ value: "First round" },
								"First round"
							),
							_react2.default.createElement(
								"option",
								{ value: "Quarter final" },
								"Quarter final"
							),
							_react2.default.createElement(
								"option",
								{ value: "Semi final" },
								"Semi final"
							),
							_react2.default.createElement(
								"option",
								{ value: "Finale" },
								"Finale"
							)
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "field-block" },
						_react2.default.createElement(
							"label",
							null,
							"Legs"
						),
						_react2.default.createElement(
							"select",
							{
								onChange: this.handleChange.bind(this, "legs"),
								value: this.state.legs
							},
							_react2.default.createElement(
								"option",
								{ value: "3" },
								"3"
							),
							_react2.default.createElement(
								"option",
								{ value: "5" },
								"5"
							),
							_react2.default.createElement(
								"option",
								{ value: "7" },
								"7"
							)
						),
						_react2.default.createElement("br", null),
						_react2.default.createElement(
							"button",
							{ type: "submit" },
							"Start new battle!"
						)
					)
				);
			}
		}]);

		return DartBattleForm;
	}(_react2.default.Component);

	exports.default = DartBattleForm;

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Reset = function (_React$Component) {
	    _inherits(Reset, _React$Component);

	    function Reset(props) {
	        _classCallCheck(this, Reset);

	        var _this = _possibleConstructorReturn(this, (Reset.__proto__ || Object.getPrototypeOf(Reset)).call(this, props));

	        _this.state = {
	            scoreA: "",
	            scoreB: ""
	        };
	        return _this;
	    }

	    _createClass(Reset, [{
	        key: "handleChange",
	        value: function handleChange(key, e) {
	            this.setState(_defineProperty({}, key, e.target.value));
	        }
	    }, {
	        key: "handleSubmit",
	        value: function handleSubmit(e) {
	            e.preventDefault();
	            this.props.handleSubmit(this.state);
	            this.setState({ scoreA: "", scoreB: "" });
	        }
	    }, {
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                "div",
	                { className: "Reset" },
	                _react2.default.createElement(
	                    "h1",
	                    null,
	                    "Master Reset"
	                ),
	                _react2.default.createElement(
	                    "form",
	                    { onSubmit: this.handleSubmit.bind(this) },
	                    _react2.default.createElement(
	                        "div",
	                        { className: "field-block" },
	                        _react2.default.createElement(
	                            "label",
	                            null,
	                            "Score Player #1"
	                        ),
	                        _react2.default.createElement("input", { value: this.state.scoreA, type: "number", onChange: this.handleChange.bind(this, "scoreA") })
	                    ),
	                    _react2.default.createElement(
	                        "div",
	                        { className: "field-block" },
	                        _react2.default.createElement(
	                            "label",
	                            null,
	                            "Score Player #2"
	                        ),
	                        _react2.default.createElement("input", { value: this.state.scoreB, type: "number", onChange: this.handleChange.bind(this, "scoreB") })
	                    ),
	                    _react2.default.createElement(
	                        "button",
	                        { type: "submit" },
	                        "Reset the scores"
	                    )
	                )
	            );
	        }
	    }]);

	    return Reset;
	}(_react2.default.Component);

	exports.default = Reset;

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = UndoButton;

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function UndoButton(props) {
	  return _react2.default.createElement(
	    "div",
	    { className: "UndoButton" },
	    _react2.default.createElement(
	      "h1",
	      null,
	      "Appletje Z"
	    ),
	    _react2.default.createElement(
	      "button",
	      { id: "appeltje-z", onClick: props.handleClick },
	      _react2.default.createElement("img", { src: "app/images/command.png", width: "40", height: "40" }),
	      _react2.default.createElement(
	        "span",
	        null,
	        "Z"
	      )
	    )
	  );
	}

/***/ }),

/***/ 63:
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var playerPool = exports.playerPool = ["Boy", "Peter", "Rogier", "Jaap", "Pieter", "Micky", "Wouter", "Nils", "Jedidja", "Willem-Jan", "Daan", "Nick", "Koen", "Bas", "Tim", "Jeffrey"];

/***/ })

});