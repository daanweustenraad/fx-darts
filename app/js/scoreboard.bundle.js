webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Scoreboard = __webpack_require__(1);

	var _Scoreboard2 = _interopRequireDefault(_Scoreboard);

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(48);

	var _reactDom2 = _interopRequireDefault(_reactDom);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var Player = function (_React$Component) {
	    _inherits(Player, _React$Component);

	    function Player() {
	        _classCallCheck(this, Player);

	        return _possibleConstructorReturn(this, (Player.__proto__ || Object.getPrototypeOf(Player)).apply(this, arguments));
	    }

	    _createClass(Player, [{
	        key: 'handleClick',
	        value: function handleClick() {
	            this.props.onUpdate(this.props.player);
	        }
	    }, {
	        key: 'render',
	        value: function render() {

	            var largestScore = Math.max.apply(Math, this.props.allPlayersWins),
	                selectedClass = this.props.selectedPlayerAName == this.props.player.name || this.props.selectedPlayerBName == this.props.player.name ? 'home-player-item active' : 'home-player-item',
	                topWinScoreClass = largestScore == this.props.player.wins ? '' : 'not-top';

	            return _react2.default.createElement(
	                'li',
	                { className: selectedClass + ' ' + topWinScoreClass, onClick: this.handleClick },
	                _react2.default.createElement(
	                    'div',
	                    { className: 'column' },
	                    _react2.default.createElement(
	                        'h2',
	                        null,
	                        this.props.player.name
	                    )
	                ),
	                _react2.default.createElement(
	                    'div',
	                    { className: 'column' },
	                    _react2.default.createElement(
	                        'h2',
	                        null,
	                        this.props.player.wins
	                    )
	                )
	            );
	        }
	    }]);

	    return Player;
	}(_react2.default.Component);

	;

	var App = function (_React$Component2) {
	    _inherits(App, _React$Component2);

	    function App(props) {
	        _classCallCheck(this, App);

	        var _this2 = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

	        _this2.state = {
	            players: [],
	            playerName: '',
	            playersEntered: false,
	            matchOn: false,
	            playerA: { 'name': '', 'wins': 0 },
	            playerB: { 'name': '', 'wins': 0 },
	            bestOf: 3,
	            timestamp: 'no timestamp yet'
	        };
	        return _this2;
	    }

	    _createClass(App, [{
	        key: 'handleSubmit',
	        value: function handleSubmit(e) {
	            e.preventDefault();

	            this.setState({ players: this.state.players.concat([{ 'name': this.state.playerName, 'wins': 0 }]), playerName: '' });
	        }
	    }, {
	        key: 'onChange',
	        value: function onChange(e) {
	            this.setState({ playerName: e.target.value });
	        }
	    }, {
	        key: 'onChangeBestOf',
	        value: function onChangeBestOf(e) {
	            this.setState({ bestOf: e.target.value });
	        }
	    }, {
	        key: 'readyBtnClickHandler',
	        value: function readyBtnClickHandler() {
	            this.setState({ playersEntered: true });
	        }
	    }, {
	        key: 'handlePlayerSelection',
	        value: function handlePlayerSelection(player) {
	            if (!player) {
	                return false;
	            }

	            if (this.state.playerA.name === '') {
	                this.setState({ playerA: { 'name': player.name } });
	            } else if (this.state.playerB.name === '') {
	                this.setState({ playerB: { 'name': player.name } });
	            } else if (this.state.playerA.name === player.name) {
	                this.setState({ playerA: { 'name': '' } });
	            } else if (this.state.playerB.name === player.name) {
	                this.setState({ playerB: { 'name': '' } });
	            }
	        }
	    }, {
	        key: 'startMatch',
	        value: function startMatch(e) {
	            e.preventDefault();
	            this.setState({ matchOn: true });
	        }
	    }, {
	        key: 'endGameHandler',
	        value: function endGameHandler(winner) {
	            var playerA = this.state.playerA;
	            var playerB = this.state.playerB;
	            playerA.name = '';
	            playerB.name = '';

	            this.setState({ matchOn: false });

	            if (winner != 'noone') {
	                this.incrementPlayersWin(winner);
	            }
	        }
	    }, {
	        key: 'incrementPlayersWin',
	        value: function incrementPlayersWin(player) {
	            if (!player) {
	                return false;
	            }

	            var oldPlayers = this.state.players;

	            for (var i in oldPlayers) {
	                if (oldPlayers[i].name === player) {
	                    oldPlayers[i].wins++;
	                    this.setState({ players: oldPlayers });
	                }
	            }
	        }
	    }, {
	        key: 'renderReadyButton',
	        value: function renderReadyButton() {
	            if (this.state.players.length < 2) {
	                return false;
	            }

	            return _react2.default.createElement(
	                'div',
	                { onClick: this.readyBtnClickHandler },
	                'hide'
	            );
	        }
	    }, {
	        key: 'renderStartScreen',
	        value: function renderStartScreen() {
	            if (!this.state.playersEntered) {
	                return _react2.default.createElement(
	                    'div',
	                    { className: 'container-player-entry' },
	                    _react2.default.createElement(
	                        'form',
	                        { className: 'player-entry-form', onSubmit: this.handleSubmit },
	                        _react2.default.createElement('input', { onChange: this.onChange, value: this.state.playerName, placeholder: 'Name' }),
	                        _react2.default.createElement(
	                            'button',
	                            null,
	                            'Add player'
	                        ),
	                        this.renderReadyButton()
	                    )
	                );
	            }

	            return _react2.default.createElement('div', null);
	        }
	    }, {
	        key: 'renderScoreboard',
	        value: function renderScoreboard() {
	            return _react2.default.createElement(_Scoreboard2.default, { scoreStart: 301, bestOf: this.state.bestOf, playerA: this.state.playerA, playerB: this.state.playerB, onEndGame: this.endGameHandler });
	        }
	    }, {
	        key: 'renderHome',
	        value: function renderHome() {
	            if (this.state.matchOn) {
	                return false;
	            }

	            var that = this,
	                playerASelectedName,
	                playerBSelectedName;

	            if (this.state.playerA.hasOwnProperty('name') && this.state.playerB.hasOwnProperty('name')) {
	                playerASelectedName = this.state.playerA.name;
	                playerBSelectedName = this.state.playerB.name;
	            }

	            var allPlayersWins = [];

	            for (var i in this.state.players) {
	                if (this.state.players[i].hasOwnProperty('wins')) {
	                    allPlayersWins.push(this.state.players[i].wins);
	                }
	            }

	            return _react2.default.createElement(
	                'div',
	                { className: 'container-home' },
	                _react2.default.createElement(
	                    'div',
	                    { className: 'home-content' },
	                    _react2.default.createElement(
	                        'ul',
	                        { className: 'home-players' },
	                        this.state.players.map(function (player) {
	                            return _react2.default.createElement(Player, { player: player, selectedPlayerAName: playerASelectedName, selectedPlayerBName: playerBSelectedName, allPlayersWins: allPlayersWins, onUpdate: that.handlePlayerSelection });
	                        })
	                    ),
	                    this.renderStartMatchBtn()
	                ),
	                _react2.default.createElement(
	                    'div',
	                    { className: 'scoreboard-footer' },
	                    _react2.default.createElement(
	                        'h1',
	                        null,
	                        'DARTS 2016'
	                    )
	                )
	            );
	        }
	    }, {
	        key: 'renderStartMatchBtn',
	        value: function renderStartMatchBtn() {
	            if (this.state.playerA.name === '' || this.state.playerB.name === '') {
	                return false;
	            }

	            return _react2.default.createElement(
	                'div',
	                { className: 'container-start-match' },
	                _react2.default.createElement(
	                    'form',
	                    { className: 'best-of-form', onSubmit: this.startMatch },
	                    _react2.default.createElement('input', { onChange: this.onChangeBestOf, value: this.state.bestOf, placeholder: 'Best of' }),
	                    _react2.default.createElement(
	                        'button',
	                        null,
	                        'START'
	                    )
	                )
	            );
	        }
	    }, {
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'div',
	                { className: 'app-inner' },
	                this.renderScoreboard()
	            );
	        }
	    }]);

	    return App;
	}(_react2.default.Component);

	_reactDom2.default.render(_react2.default.createElement(App, null), document.getElementById('app'));

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _connection = __webpack_require__(2);

	var _connection2 = _interopRequireDefault(_connection);

	var _reactTransitionGroup = __webpack_require__(22);

	var _outchart = __webpack_require__(58);

	var _outchart2 = _interopRequireDefault(_outchart);

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var top_score = 301;

	var Score = function Score(props) {
		return _react2.default.createElement(
			"div",
			{ className: "player-score" },
			_react2.default.createElement(
				"h1",
				null,
				props.score
			)
		);
	};

	var Arrow = function Arrow(props) {
		if (props.player != props.data.player) return null;

		return _react2.default.createElement(
			_reactTransitionGroup.CSSTransitionGroup,
			{
				transitionName: "fly",
				transitionEnterTimeout: 200,
				transitionLeaveTimeout: 200,
				transitionAppear: true,
				transitionAppearTimeout: 1000
			},
			_react2.default.createElement(
				"div",
				{ className: "arrow", key: "arrow-" + props.idx },
				_react2.default.createElement("img", { src: "./app/images/dart-arrow.png" }),
				_react2.default.createElement(
					"span",
					null,
					props.data.score
				)
			)
		);
	};

	var TripleArrow = function TripleArrow(props) {
		return _react2.default.createElement(
			_reactTransitionGroup.CSSTransitionGroup,
			{
				transitionName: "triple",
				transitionEnterTimeout: 200,
				transitionLeaveTimeout: 200,
				transitionAppear: true,
				transitionAppearTimeout: 700
			},
			_react2.default.createElement(
				"div",
				{ className: "arrows triple-arrow", key: "triple-arrow" },
				_react2.default.createElement(
					"div",
					{ className: "arrow" },
					_react2.default.createElement("img", { src: "./app/images/dart-arrow.png" }),
					_react2.default.createElement(
						"span",
						null,
						props.arrows[0].score
					)
				),
				_react2.default.createElement(
					"div",
					{ className: "arrow fly" },
					_react2.default.createElement("img", { src: "./app/images/triple-dart-arrow.png" }),
					_react2.default.createElement(
						"span",
						null,
						props.totalScore
					)
				),
				_react2.default.createElement(
					"div",
					{ className: "arrow" },
					_react2.default.createElement("img", { src: "./app/images/dart-arrow.png" }),
					_react2.default.createElement(
						"span",
						null,
						props.arrows[2].score
					)
				)
			)
		);
	};

	var Arrows = function Arrows(props) {
		var arrowdata = Object.keys(props.data).map(function (key) {
			return props.data[key];
		});

		if (arrowdata.length == 3 && props.player == arrowdata[0].player) {
			var totalScore = arrowdata.reduce(function (start, next) {
				return start + next.score;
			}, 0);
			return _react2.default.createElement(TripleArrow, { totalScore: totalScore, arrows: arrowdata });
		}

		return _react2.default.createElement(
			"div",
			{ className: "arrows" },
			arrowdata.map(function (arrow, idx) {
				return _react2.default.createElement(Arrow, { data: arrow, player: props.player, key: idx, idx: idx });
			})
		);
	};

	var ScoreAnimation = function ScoreAnimation(props) {
		if (!props.show) return null;

		var randomAnimation = Math.random() * 100000;

		return _react2.default.createElement(
			"div",
			{ className: "score-animation" },
			_react2.default.createElement("img", { src: "./app/images/" + props.show + ".gif?v=" + randomAnimation })
		);
	};

	var VictoryOverlay = function VictoryOverlay(props) {
		if (!props.show.active) return null;

		var random = Math.random() * 10000;
		var TOTAL_GIFS = 8;
		var randomGif = Math.floor(Math.random() * TOTAL_GIFS);

		return _react2.default.createElement("div", {
			className: "victory-overlay",
			style: {
				backgroundImage: "url('./app/images/players-agency/" + props.show.player + "Win.jpg')"
			}
		});
	};

	var Scoreboard = function (_React$Component) {
		_inherits(Scoreboard, _React$Component);

		function Scoreboard(props) {
			_classCallCheck(this, Scoreboard);

			var _this = _possibleConstructorReturn(this, (Scoreboard.__proto__ || Object.getPrototypeOf(Scoreboard)).call(this, props));

			_this.state = {
				settings: {
					playerA: { name: "...", score: top_score, legs: 0 },
					playerB: { name: "...", score: top_score, legs: 0 },
					isPlayerAsTurn: true,
					legs: 3,
					type: "...",
					legStarter: "playerA"
				},
				arrows: {},
				victory: { active: false, player: null }
			};

			_this.db = _connection2.default.fb().database();
			return _this;
		}

		_createClass(Scoreboard, [{
			key: "componentDidMount",
			value: function componentDidMount() {
				var _this2 = this;

				this.db.ref("battle/settings").on("value", function (snapshot) {
					return _this2.setState({ settings: snapshot.val() });
				});
				this.db.ref("battle/victory").on("value", function (snapshot) {
					return _this2.setState({ victory: snapshot.val() });
				});
				this.db.ref("battle/arrows").on("value", function (snapshot) {
					var arrows = snapshot.val() !== null ? snapshot.val() : {};
					_this2.setState({ arrows: arrows });
				});
			}
		}, {
			key: "getNewScore",
			value: function getNewScore(currentScore, arrowValue) {
				return currentScore - arrowValue;
			}
		}, {
			key: "getPlayersCurrentScore",
			value: function getPlayersCurrentScore(player) {
				return player.score;
			}
		}, {
			key: "getCurrentPlayersTurn",
			value: function getCurrentPlayersTurn() {
				if (this.state.settings.isPlayerAsTurn) {
					return this.state.settings.playerA;
				}

				return this.state.settings.playerB;
			}
		}, {
			key: "getBestOf",
			value: function getBestOf() {
				return _react2.default.createElement(
					"span",
					null,
					this.props.bestOf
				);
			}
		}, {
			key: "checkEndMatch",
			value: function checkEndMatch() {
				var totalLegs = this.state.settings.playerA.legs + this.state.settings.playerB.legs,
				    bestOf = this.props.bestOf,
				    minimumLegsToWin = (parseInt(bestOf) + 1) * 0.5;

				if ((this.state.settings.playerA.legs >= minimumLegsToWin || this.state.settings.playerB.legs >= minimumLegsToWin) && totalLegs >= minimumLegsToWin) {
					this.props.onEndGame(this.getCurrentPlayersTurn().name);
				}
			}
		}, {
			key: "renderOutchart",
			value: function renderOutchart() {
				var currentPlayer = this.getCurrentPlayersTurn();

				return _react2.default.createElement(_outchart2.default, {
					currentScore: this.getPlayersCurrentScore(currentPlayer),
					currentPlayer: currentPlayer
				});
			}
		}, {
			key: "renderRedDot",
			value: function renderRedDot(player) {
				if (player == this.getCurrentPlayersTurn()) {
					return _react2.default.createElement("div", { className: "current-player" });
				}

				return null;
			}
		}, {
			key: "renderFooter",
			value: function renderFooter() {
				var stage;

				if (this.props.bestOf == 5) {
					stage = "- SEMI FINAL";
				}

				if (this.props.bestOf == 7) {
					stage = "- FINAL";
				}

				return _react2.default.createElement(
					"div",
					{ className: "scoreboard-footer" },
					_react2.default.createElement(
						"h1",
						null,
						"FX Open 2019 -",
						" ",
						_react2.default.createElement(
							"span",
							{ className: "battleType" },
							this.state.settings.type
						)
					)
				);
			}
		}, {
			key: "isInt",
			value: function isInt(value) {
				return !isNaN(value) && function (x) {
					return (x | 0) === x;
				}(parseFloat(value));
			}
		}, {
			key: "handleGoBack",
			value: function handleGoBack() {
				this.props.onEndGame("noone");
			}
		}, {
			key: "pickAnimation",
			value: function pickAnimation(tempScore, thrownScore) {
				if (tempScore == 0) {
					return "finish";
				} else if (tempScore < 1) {
					return "death";
				}
				return false;
			}
		}, {
			key: "isPlayerFinished",
			value: function isPlayerFinished(player) {
				var _this3 = this;

				var currentPlayer = this.state.settings.isPlayerAsTurn ? "playerA" : "playerB";
				if (currentPlayer != player) return false;

				var currentScore = this.state.settings[player].score;
				var shouldCountArrows = Object.keys(this.state.arrows).length > 0;
				var tempScore = void 0;
				var thrownScore = void 0;

				if (shouldCountArrows) {
					thrownScore = Object.keys(this.state.arrows).filter(function (key) {
						return _this3.state.arrows[key].player == currentPlayer;
					}).reduce(function (start, next) {
						return start + _this3.state.arrows[next].score;
					}, 0);

					tempScore = currentScore - thrownScore;
				} else {
					tempScore = currentScore;
				}
				return this.pickAnimation(tempScore, thrownScore);
			}
		}, {
			key: "render",
			value: function render() {
				return _react2.default.createElement(
					"div",
					{ className: "container-scoreboard" },
					_react2.default.createElement(
						"div",
						{ className: "scoreboard-header" },
						_react2.default.createElement(
							"div",
							{ className: "scoreboard-header--left" },
							_react2.default.createElement(
								"h1",
								null,
								"Best of ",
								this.state.settings.legs,
								" ",
								this.state.settings.legs == 1 ? "leg" : "legs"
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "scoreboard-header--right" },
							_react2.default.createElement(
								"h1",
								null,
								"Legs"
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "scoreboard-header--right" },
							_react2.default.createElement(
								"h1",
								null,
								"Pts"
							)
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "scoreboard-content" },
						_react2.default.createElement(VictoryOverlay, { show: this.state.victory }),
						_react2.default.createElement(
							"div",
							{ className: "player-row player-row--a" },
							_react2.default.createElement(
								"div",
								{ className: "player-name" },
								_react2.default.createElement(
									"h1",
									null,
									this.state.settings.playerA.name
								),
								this.renderRedDot(this.state.settings.playerA)
							),
							_react2.default.createElement(Arrows, { data: this.state.arrows, player: "playerA" }),
							_react2.default.createElement(
								"div",
								{
									className: this.state.settings.legStarter == "playerA" ? "player-stats starter" : "player-stats"
								},
								_react2.default.createElement(
									"div",
									{ className: "player-legs" },
									_react2.default.createElement(
										"h1",
										null,
										this.state.settings.playerA.legs
									)
								),
								_react2.default.createElement(Score, { score: this.state.settings.playerA.score }),
								_react2.default.createElement(_outchart2.default, {
									player: "playerA",
									currentScore: this.state.settings.playerA.score,
									arrows: this.state.settings.isPlayerAsTurn ? this.state.arrows : {}
								}),
								_react2.default.createElement(ScoreAnimation, {
									show: this.isPlayerFinished("playerA")
								})
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "player-row player-row--b" },
							_react2.default.createElement(
								"div",
								{ className: "player-name" },
								_react2.default.createElement(
									"h1",
									{
										className: this.state.settings.legStarter == "playerB" ? "underline" : ""
									},
									this.state.settings.playerB.name
								),
								this.renderRedDot(this.state.settings.playerB)
							),
							_react2.default.createElement(Arrows, { data: this.state.arrows, player: "playerB" }),
							_react2.default.createElement(
								"div",
								{
									className: this.state.settings.legStarter == "playerB" ? "player-stats starter" : "player-stats"
								},
								_react2.default.createElement(
									"div",
									{ className: "player-legs" },
									_react2.default.createElement(
										"h1",
										null,
										this.state.settings.playerB.legs
									)
								),
								_react2.default.createElement(Score, { score: this.state.settings.playerB.score }),
								_react2.default.createElement(_outchart2.default, {
									player: "playerB",
									currentScore: this.state.settings.playerB.score,
									arrows: this.state.settings.isPlayerAsTurn ? {} : this.state.arrows
								}),
								_react2.default.createElement(ScoreAnimation, {
									show: this.isPlayerFinished("playerB")
								})
							)
						)
					),
					this.renderFooter()
				);
			}
		}]);

		return Scoreboard;
	}(_react2.default.Component);

	exports.default = Scoreboard;

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _CSSTransitionGroup = __webpack_require__(23);

	var _CSSTransitionGroup2 = _interopRequireDefault(_CSSTransitionGroup);

	var _TransitionGroup = __webpack_require__(36);

	var _TransitionGroup2 = _interopRequireDefault(_TransitionGroup);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	module.exports = {
	  TransitionGroup: _TransitionGroup2.default,
	  CSSTransitionGroup: _CSSTransitionGroup2.default
	};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	exports.__esModule = true;

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _propTypes = __webpack_require__(30);

	var _propTypes2 = _interopRequireDefault(_propTypes);

	var _TransitionGroup = __webpack_require__(36);

	var _TransitionGroup2 = _interopRequireDefault(_TransitionGroup);

	var _CSSTransitionGroupChild = __webpack_require__(40);

	var _CSSTransitionGroupChild2 = _interopRequireDefault(_CSSTransitionGroupChild);

	var _PropTypes = __webpack_require__(57);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var propTypes = {
	  transitionName: _PropTypes.nameShape.isRequired,

	  transitionAppear: _propTypes2.default.bool,
	  transitionEnter: _propTypes2.default.bool,
	  transitionLeave: _propTypes2.default.bool,
	  transitionAppearTimeout: (0, _PropTypes.transitionTimeout)('Appear'),
	  transitionEnterTimeout: (0, _PropTypes.transitionTimeout)('Enter'),
	  transitionLeaveTimeout: (0, _PropTypes.transitionTimeout)('Leave')
	};

	var defaultProps = {
	  transitionAppear: false,
	  transitionEnter: true,
	  transitionLeave: true
	};

	var CSSTransitionGroup = function (_React$Component) {
	  _inherits(CSSTransitionGroup, _React$Component);

	  function CSSTransitionGroup() {
	    var _temp, _this, _ret;

	    _classCallCheck(this, CSSTransitionGroup);

	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this._wrapChild = function (child) {
	      return _react2.default.createElement(_CSSTransitionGroupChild2.default, {
	        name: _this.props.transitionName,
	        appear: _this.props.transitionAppear,
	        enter: _this.props.transitionEnter,
	        leave: _this.props.transitionLeave,
	        appearTimeout: _this.props.transitionAppearTimeout,
	        enterTimeout: _this.props.transitionEnterTimeout,
	        leaveTimeout: _this.props.transitionLeaveTimeout
	      }, child);
	    }, _temp), _possibleConstructorReturn(_this, _ret);
	  }

	  // We need to provide this childFactory so that
	  // ReactCSSTransitionGroupChild can receive updates to name, enter, and
	  // leave while it is leaving.


	  CSSTransitionGroup.prototype.render = function render() {
	    return _react2.default.createElement(_TransitionGroup2.default, _extends({}, this.props, { childFactory: this._wrapChild }));
	  };

	  return CSSTransitionGroup;
	}(_react2.default.Component);

	CSSTransitionGroup.displayName = 'CSSTransitionGroup';


	CSSTransitionGroup.propTypes = process.env.NODE_ENV !== "production" ? propTypes : {};
	CSSTransitionGroup.defaultProps = defaultProps;

	exports.default = CSSTransitionGroup;
	module.exports = exports['default'];
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	if (process.env.NODE_ENV !== 'production') {
	  var ReactIs = __webpack_require__(31);

	  // By explicitly using `prop-types` you are opting into new development behavior.
	  // http://fb.me/prop-types-in-prod
	  var throwOnDirectAccess = true;
	  module.exports = __webpack_require__(34)(ReactIs.isElement, throwOnDirectAccess);
	} else {
	  // By explicitly using `prop-types` you are opting into new production behavior.
	  // http://fb.me/prop-types-in-prod
	  module.exports = __webpack_require__(35)();
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	if (process.env.NODE_ENV === 'production') {
	  module.exports = __webpack_require__(32);
	} else {
	  module.exports = __webpack_require__(33);
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 32 */
/***/ (function(module, exports) {

	/** @license React v16.10.2
	 * react-is.production.min.js
	 *
	 * Copyright (c) Facebook, Inc. and its affiliates.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	'use strict';Object.defineProperty(exports,"__esModule",{value:!0});
	var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?Symbol.for("react.suspense_list"):
	60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.fundamental"):60117,w=b?Symbol.for("react.responder"):60118,x=b?Symbol.for("react.scope"):60119;function y(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case h:return a;default:return u}}case t:case r:case d:return u}}}function z(a){return y(a)===m}
	exports.typeOf=y;exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;exports.Fragment=e;exports.Lazy=t;exports.Memo=r;exports.Portal=d;exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;
	exports.isValidElementType=function(a){return"string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===v||a.$$typeof===w||a.$$typeof===x)};exports.isAsyncMode=function(a){return z(a)||y(a)===l};exports.isConcurrentMode=z;exports.isContextConsumer=function(a){return y(a)===k};exports.isContextProvider=function(a){return y(a)===h};
	exports.isElement=function(a){return"object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return y(a)===n};exports.isFragment=function(a){return y(a)===e};exports.isLazy=function(a){return y(a)===t};exports.isMemo=function(a){return y(a)===r};exports.isPortal=function(a){return y(a)===d};exports.isProfiler=function(a){return y(a)===g};exports.isStrictMode=function(a){return y(a)===f};exports.isSuspense=function(a){return y(a)===p};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/** @license React v16.10.2
	 * react-is.development.js
	 *
	 * Copyright (c) Facebook, Inc. and its affiliates.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	'use strict';



	if (process.env.NODE_ENV !== "production") {
	  (function() {
	'use strict';

	Object.defineProperty(exports, '__esModule', { value: true });

	// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
	// nor polyfill, then a plain number is used for performance.
	var hasSymbol = typeof Symbol === 'function' && Symbol.for;
	var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
	var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
	var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
	var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
	var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
	var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
	var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace; // TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
	// (unstable) APIs that have been removed. Can we remove the symbols?

	var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
	var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
	var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
	var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
	var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
	var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
	var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
	var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
	var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;
	var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for('react.scope') : 0xead7;

	function isValidElementType(type) {
	  return typeof type === 'string' || typeof type === 'function' || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
	  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE);
	}

	/**
	 * Forked from fbjs/warning:
	 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
	 *
	 * Only change is we use console.warn instead of console.error,
	 * and do nothing when 'console' is not supported.
	 * This really simplifies the code.
	 * ---
	 * Similar to invariant but only logs a warning if the condition is not met.
	 * This can be used to log issues in development environments in critical
	 * paths. Removing the logging code for production environments will keep the
	 * same logic and follow the same code paths.
	 */
	var lowPriorityWarningWithoutStack = function () {};

	{
	  var printWarning = function (format) {
	    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	      args[_key - 1] = arguments[_key];
	    }

	    var argIndex = 0;
	    var message = 'Warning: ' + format.replace(/%s/g, function () {
	      return args[argIndex++];
	    });

	    if (typeof console !== 'undefined') {
	      console.warn(message);
	    }

	    try {
	      // --- Welcome to debugging React ---
	      // This error was thrown as a convenience so that you can use this stack
	      // to find the callsite that caused this warning to fire.
	      throw new Error(message);
	    } catch (x) {}
	  };

	  lowPriorityWarningWithoutStack = function (condition, format) {
	    if (format === undefined) {
	      throw new Error('`lowPriorityWarningWithoutStack(condition, format, ...args)` requires a warning ' + 'message argument');
	    }

	    if (!condition) {
	      for (var _len2 = arguments.length, args = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
	        args[_key2 - 2] = arguments[_key2];
	      }

	      printWarning.apply(void 0, [format].concat(args));
	    }
	  };
	}

	var lowPriorityWarningWithoutStack$1 = lowPriorityWarningWithoutStack;

	function typeOf(object) {
	  if (typeof object === 'object' && object !== null) {
	    var $$typeof = object.$$typeof;

	    switch ($$typeof) {
	      case REACT_ELEMENT_TYPE:
	        var type = object.type;

	        switch (type) {
	          case REACT_ASYNC_MODE_TYPE:
	          case REACT_CONCURRENT_MODE_TYPE:
	          case REACT_FRAGMENT_TYPE:
	          case REACT_PROFILER_TYPE:
	          case REACT_STRICT_MODE_TYPE:
	          case REACT_SUSPENSE_TYPE:
	            return type;

	          default:
	            var $$typeofType = type && type.$$typeof;

	            switch ($$typeofType) {
	              case REACT_CONTEXT_TYPE:
	              case REACT_FORWARD_REF_TYPE:
	              case REACT_PROVIDER_TYPE:
	                return $$typeofType;

	              default:
	                return $$typeof;
	            }

	        }

	      case REACT_LAZY_TYPE:
	      case REACT_MEMO_TYPE:
	      case REACT_PORTAL_TYPE:
	        return $$typeof;
	    }
	  }

	  return undefined;
	} // AsyncMode is deprecated along with isAsyncMode

	var AsyncMode = REACT_ASYNC_MODE_TYPE;
	var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
	var ContextConsumer = REACT_CONTEXT_TYPE;
	var ContextProvider = REACT_PROVIDER_TYPE;
	var Element = REACT_ELEMENT_TYPE;
	var ForwardRef = REACT_FORWARD_REF_TYPE;
	var Fragment = REACT_FRAGMENT_TYPE;
	var Lazy = REACT_LAZY_TYPE;
	var Memo = REACT_MEMO_TYPE;
	var Portal = REACT_PORTAL_TYPE;
	var Profiler = REACT_PROFILER_TYPE;
	var StrictMode = REACT_STRICT_MODE_TYPE;
	var Suspense = REACT_SUSPENSE_TYPE;
	var hasWarnedAboutDeprecatedIsAsyncMode = false; // AsyncMode should be deprecated

	function isAsyncMode(object) {
	  {
	    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
	      hasWarnedAboutDeprecatedIsAsyncMode = true;
	      lowPriorityWarningWithoutStack$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
	    }
	  }

	  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
	}
	function isConcurrentMode(object) {
	  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
	}
	function isContextConsumer(object) {
	  return typeOf(object) === REACT_CONTEXT_TYPE;
	}
	function isContextProvider(object) {
	  return typeOf(object) === REACT_PROVIDER_TYPE;
	}
	function isElement(object) {
	  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
	}
	function isForwardRef(object) {
	  return typeOf(object) === REACT_FORWARD_REF_TYPE;
	}
	function isFragment(object) {
	  return typeOf(object) === REACT_FRAGMENT_TYPE;
	}
	function isLazy(object) {
	  return typeOf(object) === REACT_LAZY_TYPE;
	}
	function isMemo(object) {
	  return typeOf(object) === REACT_MEMO_TYPE;
	}
	function isPortal(object) {
	  return typeOf(object) === REACT_PORTAL_TYPE;
	}
	function isProfiler(object) {
	  return typeOf(object) === REACT_PROFILER_TYPE;
	}
	function isStrictMode(object) {
	  return typeOf(object) === REACT_STRICT_MODE_TYPE;
	}
	function isSuspense(object) {
	  return typeOf(object) === REACT_SUSPENSE_TYPE;
	}

	exports.typeOf = typeOf;
	exports.AsyncMode = AsyncMode;
	exports.ConcurrentMode = ConcurrentMode;
	exports.ContextConsumer = ContextConsumer;
	exports.ContextProvider = ContextProvider;
	exports.Element = Element;
	exports.ForwardRef = ForwardRef;
	exports.Fragment = Fragment;
	exports.Lazy = Lazy;
	exports.Memo = Memo;
	exports.Portal = Portal;
	exports.Profiler = Profiler;
	exports.StrictMode = StrictMode;
	exports.Suspense = Suspense;
	exports.isValidElementType = isValidElementType;
	exports.isAsyncMode = isAsyncMode;
	exports.isConcurrentMode = isConcurrentMode;
	exports.isContextConsumer = isContextConsumer;
	exports.isContextProvider = isContextProvider;
	exports.isElement = isElement;
	exports.isForwardRef = isForwardRef;
	exports.isFragment = isFragment;
	exports.isLazy = isLazy;
	exports.isMemo = isMemo;
	exports.isPortal = isPortal;
	exports.isProfiler = isProfiler;
	exports.isStrictMode = isStrictMode;
	exports.isSuspense = isSuspense;
	  })();
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	'use strict';

	var ReactIs = __webpack_require__(31);
	var assign = __webpack_require__(26);

	var ReactPropTypesSecret = __webpack_require__(29);
	var checkPropTypes = __webpack_require__(28);

	var has = Function.call.bind(Object.prototype.hasOwnProperty);
	var printWarning = function() {};

	if (process.env.NODE_ENV !== 'production') {
	  printWarning = function(text) {
	    var message = 'Warning: ' + text;
	    if (typeof console !== 'undefined') {
	      console.error(message);
	    }
	    try {
	      // --- Welcome to debugging React ---
	      // This error was thrown as a convenience so that you can use this stack
	      // to find the callsite that caused this warning to fire.
	      throw new Error(message);
	    } catch (x) {}
	  };
	}

	function emptyFunctionThatReturnsNull() {
	  return null;
	}

	module.exports = function(isValidElement, throwOnDirectAccess) {
	  /* global Symbol */
	  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
	  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

	  /**
	   * Returns the iterator method function contained on the iterable object.
	   *
	   * Be sure to invoke the function with the iterable as context:
	   *
	   *     var iteratorFn = getIteratorFn(myIterable);
	   *     if (iteratorFn) {
	   *       var iterator = iteratorFn.call(myIterable);
	   *       ...
	   *     }
	   *
	   * @param {?object} maybeIterable
	   * @return {?function}
	   */
	  function getIteratorFn(maybeIterable) {
	    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
	    if (typeof iteratorFn === 'function') {
	      return iteratorFn;
	    }
	  }

	  /**
	   * Collection of methods that allow declaration and validation of props that are
	   * supplied to React components. Example usage:
	   *
	   *   var Props = require('ReactPropTypes');
	   *   var MyArticle = React.createClass({
	   *     propTypes: {
	   *       // An optional string prop named "description".
	   *       description: Props.string,
	   *
	   *       // A required enum prop named "category".
	   *       category: Props.oneOf(['News','Photos']).isRequired,
	   *
	   *       // A prop named "dialog" that requires an instance of Dialog.
	   *       dialog: Props.instanceOf(Dialog).isRequired
	   *     },
	   *     render: function() { ... }
	   *   });
	   *
	   * A more formal specification of how these methods are used:
	   *
	   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
	   *   decl := ReactPropTypes.{type}(.isRequired)?
	   *
	   * Each and every declaration produces a function with the same signature. This
	   * allows the creation of custom validation functions. For example:
	   *
	   *  var MyLink = React.createClass({
	   *    propTypes: {
	   *      // An optional string or URI prop named "href".
	   *      href: function(props, propName, componentName) {
	   *        var propValue = props[propName];
	   *        if (propValue != null && typeof propValue !== 'string' &&
	   *            !(propValue instanceof URI)) {
	   *          return new Error(
	   *            'Expected a string or an URI for ' + propName + ' in ' +
	   *            componentName
	   *          );
	   *        }
	   *      }
	   *    },
	   *    render: function() {...}
	   *  });
	   *
	   * @internal
	   */

	  var ANONYMOUS = '<<anonymous>>';

	  // Important!
	  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
	  var ReactPropTypes = {
	    array: createPrimitiveTypeChecker('array'),
	    bool: createPrimitiveTypeChecker('boolean'),
	    func: createPrimitiveTypeChecker('function'),
	    number: createPrimitiveTypeChecker('number'),
	    object: createPrimitiveTypeChecker('object'),
	    string: createPrimitiveTypeChecker('string'),
	    symbol: createPrimitiveTypeChecker('symbol'),

	    any: createAnyTypeChecker(),
	    arrayOf: createArrayOfTypeChecker,
	    element: createElementTypeChecker(),
	    elementType: createElementTypeTypeChecker(),
	    instanceOf: createInstanceTypeChecker,
	    node: createNodeChecker(),
	    objectOf: createObjectOfTypeChecker,
	    oneOf: createEnumTypeChecker,
	    oneOfType: createUnionTypeChecker,
	    shape: createShapeTypeChecker,
	    exact: createStrictShapeTypeChecker,
	  };

	  /**
	   * inlined Object.is polyfill to avoid requiring consumers ship their own
	   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
	   */
	  /*eslint-disable no-self-compare*/
	  function is(x, y) {
	    // SameValue algorithm
	    if (x === y) {
	      // Steps 1-5, 7-10
	      // Steps 6.b-6.e: +0 != -0
	      return x !== 0 || 1 / x === 1 / y;
	    } else {
	      // Step 6.a: NaN == NaN
	      return x !== x && y !== y;
	    }
	  }
	  /*eslint-enable no-self-compare*/

	  /**
	   * We use an Error-like object for backward compatibility as people may call
	   * PropTypes directly and inspect their output. However, we don't use real
	   * Errors anymore. We don't inspect their stack anyway, and creating them
	   * is prohibitively expensive if they are created too often, such as what
	   * happens in oneOfType() for any type before the one that matched.
	   */
	  function PropTypeError(message) {
	    this.message = message;
	    this.stack = '';
	  }
	  // Make `instanceof Error` still work for returned errors.
	  PropTypeError.prototype = Error.prototype;

	  function createChainableTypeChecker(validate) {
	    if (process.env.NODE_ENV !== 'production') {
	      var manualPropTypeCallCache = {};
	      var manualPropTypeWarningCount = 0;
	    }
	    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
	      componentName = componentName || ANONYMOUS;
	      propFullName = propFullName || propName;

	      if (secret !== ReactPropTypesSecret) {
	        if (throwOnDirectAccess) {
	          // New behavior only for users of `prop-types` package
	          var err = new Error(
	            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
	            'Use `PropTypes.checkPropTypes()` to call them. ' +
	            'Read more at http://fb.me/use-check-prop-types'
	          );
	          err.name = 'Invariant Violation';
	          throw err;
	        } else if (process.env.NODE_ENV !== 'production' && typeof console !== 'undefined') {
	          // Old behavior for people using React.PropTypes
	          var cacheKey = componentName + ':' + propName;
	          if (
	            !manualPropTypeCallCache[cacheKey] &&
	            // Avoid spamming the console because they are often not actionable except for lib authors
	            manualPropTypeWarningCount < 3
	          ) {
	            printWarning(
	              'You are manually calling a React.PropTypes validation ' +
	              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
	              'and will throw in the standalone `prop-types` package. ' +
	              'You may be seeing this warning due to a third-party PropTypes ' +
	              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
	            );
	            manualPropTypeCallCache[cacheKey] = true;
	            manualPropTypeWarningCount++;
	          }
	        }
	      }
	      if (props[propName] == null) {
	        if (isRequired) {
	          if (props[propName] === null) {
	            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
	          }
	          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
	        }
	        return null;
	      } else {
	        return validate(props, propName, componentName, location, propFullName);
	      }
	    }

	    var chainedCheckType = checkType.bind(null, false);
	    chainedCheckType.isRequired = checkType.bind(null, true);

	    return chainedCheckType;
	  }

	  function createPrimitiveTypeChecker(expectedType) {
	    function validate(props, propName, componentName, location, propFullName, secret) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== expectedType) {
	        // `propValue` being instance of, say, date/regexp, pass the 'object'
	        // check, but we can offer a more precise error message here rather than
	        // 'of type `object`'.
	        var preciseType = getPreciseType(propValue);

	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createAnyTypeChecker() {
	    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
	  }

	  function createArrayOfTypeChecker(typeChecker) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (typeof typeChecker !== 'function') {
	        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
	      }
	      var propValue = props[propName];
	      if (!Array.isArray(propValue)) {
	        var propType = getPropType(propValue);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
	      }
	      for (var i = 0; i < propValue.length; i++) {
	        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
	        if (error instanceof Error) {
	          return error;
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createElementTypeChecker() {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      if (!isValidElement(propValue)) {
	        var propType = getPropType(propValue);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createElementTypeTypeChecker() {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      if (!ReactIs.isValidElementType(propValue)) {
	        var propType = getPropType(propValue);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createInstanceTypeChecker(expectedClass) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (!(props[propName] instanceof expectedClass)) {
	        var expectedClassName = expectedClass.name || ANONYMOUS;
	        var actualClassName = getClassName(props[propName]);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createEnumTypeChecker(expectedValues) {
	    if (!Array.isArray(expectedValues)) {
	      if (process.env.NODE_ENV !== 'production') {
	        if (arguments.length > 1) {
	          printWarning(
	            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
	            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
	          );
	        } else {
	          printWarning('Invalid argument supplied to oneOf, expected an array.');
	        }
	      }
	      return emptyFunctionThatReturnsNull;
	    }

	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      for (var i = 0; i < expectedValues.length; i++) {
	        if (is(propValue, expectedValues[i])) {
	          return null;
	        }
	      }

	      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
	        var type = getPreciseType(value);
	        if (type === 'symbol') {
	          return String(value);
	        }
	        return value;
	      });
	      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createObjectOfTypeChecker(typeChecker) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (typeof typeChecker !== 'function') {
	        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
	      }
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
	      }
	      for (var key in propValue) {
	        if (has(propValue, key)) {
	          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
	          if (error instanceof Error) {
	            return error;
	          }
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createUnionTypeChecker(arrayOfTypeCheckers) {
	    if (!Array.isArray(arrayOfTypeCheckers)) {
	      process.env.NODE_ENV !== 'production' ? printWarning('Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
	      return emptyFunctionThatReturnsNull;
	    }

	    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
	      var checker = arrayOfTypeCheckers[i];
	      if (typeof checker !== 'function') {
	        printWarning(
	          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
	          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
	        );
	        return emptyFunctionThatReturnsNull;
	      }
	    }

	    function validate(props, propName, componentName, location, propFullName) {
	      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
	        var checker = arrayOfTypeCheckers[i];
	        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
	          return null;
	        }
	      }

	      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createNodeChecker() {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (!isNode(props[propName])) {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createShapeTypeChecker(shapeTypes) {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
	      }
	      for (var key in shapeTypes) {
	        var checker = shapeTypes[key];
	        if (!checker) {
	          continue;
	        }
	        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
	        if (error) {
	          return error;
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createStrictShapeTypeChecker(shapeTypes) {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
	      }
	      // We need to check all keys in case some are required but missing from
	      // props.
	      var allKeys = assign({}, props[propName], shapeTypes);
	      for (var key in allKeys) {
	        var checker = shapeTypes[key];
	        if (!checker) {
	          return new PropTypeError(
	            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
	            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
	            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
	          );
	        }
	        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
	        if (error) {
	          return error;
	        }
	      }
	      return null;
	    }

	    return createChainableTypeChecker(validate);
	  }

	  function isNode(propValue) {
	    switch (typeof propValue) {
	      case 'number':
	      case 'string':
	      case 'undefined':
	        return true;
	      case 'boolean':
	        return !propValue;
	      case 'object':
	        if (Array.isArray(propValue)) {
	          return propValue.every(isNode);
	        }
	        if (propValue === null || isValidElement(propValue)) {
	          return true;
	        }

	        var iteratorFn = getIteratorFn(propValue);
	        if (iteratorFn) {
	          var iterator = iteratorFn.call(propValue);
	          var step;
	          if (iteratorFn !== propValue.entries) {
	            while (!(step = iterator.next()).done) {
	              if (!isNode(step.value)) {
	                return false;
	              }
	            }
	          } else {
	            // Iterator will provide entry [k,v] tuples rather than values.
	            while (!(step = iterator.next()).done) {
	              var entry = step.value;
	              if (entry) {
	                if (!isNode(entry[1])) {
	                  return false;
	                }
	              }
	            }
	          }
	        } else {
	          return false;
	        }

	        return true;
	      default:
	        return false;
	    }
	  }

	  function isSymbol(propType, propValue) {
	    // Native Symbol.
	    if (propType === 'symbol') {
	      return true;
	    }

	    // falsy value can't be a Symbol
	    if (!propValue) {
	      return false;
	    }

	    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
	    if (propValue['@@toStringTag'] === 'Symbol') {
	      return true;
	    }

	    // Fallback for non-spec compliant Symbols which are polyfilled.
	    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
	      return true;
	    }

	    return false;
	  }

	  // Equivalent of `typeof` but with special handling for array and regexp.
	  function getPropType(propValue) {
	    var propType = typeof propValue;
	    if (Array.isArray(propValue)) {
	      return 'array';
	    }
	    if (propValue instanceof RegExp) {
	      // Old webkits (at least until Android 4.0) return 'function' rather than
	      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
	      // passes PropTypes.object.
	      return 'object';
	    }
	    if (isSymbol(propType, propValue)) {
	      return 'symbol';
	    }
	    return propType;
	  }

	  // This handles more types than `getPropType`. Only used for error messages.
	  // See `createPrimitiveTypeChecker`.
	  function getPreciseType(propValue) {
	    if (typeof propValue === 'undefined' || propValue === null) {
	      return '' + propValue;
	    }
	    var propType = getPropType(propValue);
	    if (propType === 'object') {
	      if (propValue instanceof Date) {
	        return 'date';
	      } else if (propValue instanceof RegExp) {
	        return 'regexp';
	      }
	    }
	    return propType;
	  }

	  // Returns a string that is postfixed to a warning about an invalid type.
	  // For example, "undefined" or "of type array"
	  function getPostfixForTypeWarning(value) {
	    var type = getPreciseType(value);
	    switch (type) {
	      case 'array':
	      case 'object':
	        return 'an ' + type;
	      case 'boolean':
	      case 'date':
	      case 'regexp':
	        return 'a ' + type;
	      default:
	        return type;
	    }
	  }

	  // Returns class name of the object, if any.
	  function getClassName(propValue) {
	    if (!propValue.constructor || !propValue.constructor.name) {
	      return ANONYMOUS;
	    }
	    return propValue.constructor.name;
	  }

	  ReactPropTypes.checkPropTypes = checkPropTypes;
	  ReactPropTypes.resetWarningCache = checkPropTypes.resetWarningCache;
	  ReactPropTypes.PropTypes = ReactPropTypes;

	  return ReactPropTypes;
	};

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	'use strict';

	var ReactPropTypesSecret = __webpack_require__(29);

	function emptyFunction() {}
	function emptyFunctionWithReset() {}
	emptyFunctionWithReset.resetWarningCache = emptyFunction;

	module.exports = function() {
	  function shim(props, propName, componentName, location, propFullName, secret) {
	    if (secret === ReactPropTypesSecret) {
	      // It is still safe when called from React.
	      return;
	    }
	    var err = new Error(
	      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
	      'Use PropTypes.checkPropTypes() to call them. ' +
	      'Read more at http://fb.me/use-check-prop-types'
	    );
	    err.name = 'Invariant Violation';
	    throw err;
	  };
	  shim.isRequired = shim;
	  function getShim() {
	    return shim;
	  };
	  // Important!
	  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
	  var ReactPropTypes = {
	    array: shim,
	    bool: shim,
	    func: shim,
	    number: shim,
	    object: shim,
	    string: shim,
	    symbol: shim,

	    any: shim,
	    arrayOf: getShim,
	    element: shim,
	    elementType: shim,
	    instanceOf: getShim,
	    node: shim,
	    objectOf: getShim,
	    oneOf: getShim,
	    oneOfType: getShim,
	    shape: getShim,
	    exact: getShim,

	    checkPropTypes: emptyFunctionWithReset,
	    resetWarningCache: emptyFunction
	  };

	  ReactPropTypes.PropTypes = ReactPropTypes;

	  return ReactPropTypes;
	};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	exports.__esModule = true;

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _chainFunction = __webpack_require__(37);

	var _chainFunction2 = _interopRequireDefault(_chainFunction);

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _propTypes = __webpack_require__(30);

	var _propTypes2 = _interopRequireDefault(_propTypes);

	var _warning = __webpack_require__(38);

	var _warning2 = _interopRequireDefault(_warning);

	var _ChildMapping = __webpack_require__(39);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var propTypes = {
	  component: _propTypes2.default.any,
	  childFactory: _propTypes2.default.func,
	  children: _propTypes2.default.node
	};

	var defaultProps = {
	  component: 'span',
	  childFactory: function childFactory(child) {
	    return child;
	  }
	};

	var TransitionGroup = function (_React$Component) {
	  _inherits(TransitionGroup, _React$Component);

	  function TransitionGroup(props, context) {
	    _classCallCheck(this, TransitionGroup);

	    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props, context));

	    _this.performAppear = function (key, component) {
	      _this.currentlyTransitioningKeys[key] = true;

	      if (component.componentWillAppear) {
	        component.componentWillAppear(_this._handleDoneAppearing.bind(_this, key, component));
	      } else {
	        _this._handleDoneAppearing(key, component);
	      }
	    };

	    _this._handleDoneAppearing = function (key, component) {
	      if (component.componentDidAppear) {
	        component.componentDidAppear();
	      }

	      delete _this.currentlyTransitioningKeys[key];

	      var currentChildMapping = (0, _ChildMapping.getChildMapping)(_this.props.children);

	      if (!currentChildMapping || !currentChildMapping.hasOwnProperty(key)) {
	        // This was removed before it had fully appeared. Remove it.
	        _this.performLeave(key, component);
	      }
	    };

	    _this.performEnter = function (key, component) {
	      _this.currentlyTransitioningKeys[key] = true;

	      if (component.componentWillEnter) {
	        component.componentWillEnter(_this._handleDoneEntering.bind(_this, key, component));
	      } else {
	        _this._handleDoneEntering(key, component);
	      }
	    };

	    _this._handleDoneEntering = function (key, component) {
	      if (component.componentDidEnter) {
	        component.componentDidEnter();
	      }

	      delete _this.currentlyTransitioningKeys[key];

	      var currentChildMapping = (0, _ChildMapping.getChildMapping)(_this.props.children);

	      if (!currentChildMapping || !currentChildMapping.hasOwnProperty(key)) {
	        // This was removed before it had fully entered. Remove it.
	        _this.performLeave(key, component);
	      }
	    };

	    _this.performLeave = function (key, component) {
	      _this.currentlyTransitioningKeys[key] = true;

	      if (component.componentWillLeave) {
	        component.componentWillLeave(_this._handleDoneLeaving.bind(_this, key, component));
	      } else {
	        // Note that this is somewhat dangerous b/c it calls setState()
	        // again, effectively mutating the component before all the work
	        // is done.
	        _this._handleDoneLeaving(key, component);
	      }
	    };

	    _this._handleDoneLeaving = function (key, component) {
	      if (component.componentDidLeave) {
	        component.componentDidLeave();
	      }

	      delete _this.currentlyTransitioningKeys[key];

	      var currentChildMapping = (0, _ChildMapping.getChildMapping)(_this.props.children);

	      if (currentChildMapping && currentChildMapping.hasOwnProperty(key)) {
	        // This entered again before it fully left. Add it again.
	        _this.keysToEnter.push(key);
	      } else {
	        _this.setState(function (state) {
	          var newChildren = _extends({}, state.children);
	          delete newChildren[key];
	          return { children: newChildren };
	        });
	      }
	    };

	    _this.childRefs = Object.create(null);

	    _this.state = {
	      children: (0, _ChildMapping.getChildMapping)(props.children)
	    };
	    return _this;
	  }

	  TransitionGroup.prototype.componentWillMount = function componentWillMount() {
	    this.currentlyTransitioningKeys = {};
	    this.keysToEnter = [];
	    this.keysToLeave = [];
	  };

	  TransitionGroup.prototype.componentDidMount = function componentDidMount() {
	    var initialChildMapping = this.state.children;
	    for (var key in initialChildMapping) {
	      if (initialChildMapping[key]) {
	        this.performAppear(key, this.childRefs[key]);
	      }
	    }
	  };

	  TransitionGroup.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
	    var nextChildMapping = (0, _ChildMapping.getChildMapping)(nextProps.children);
	    var prevChildMapping = this.state.children;

	    this.setState({
	      children: (0, _ChildMapping.mergeChildMappings)(prevChildMapping, nextChildMapping)
	    });

	    for (var key in nextChildMapping) {
	      var hasPrev = prevChildMapping && prevChildMapping.hasOwnProperty(key);
	      if (nextChildMapping[key] && !hasPrev && !this.currentlyTransitioningKeys[key]) {
	        this.keysToEnter.push(key);
	      }
	    }

	    for (var _key in prevChildMapping) {
	      var hasNext = nextChildMapping && nextChildMapping.hasOwnProperty(_key);
	      if (prevChildMapping[_key] && !hasNext && !this.currentlyTransitioningKeys[_key]) {
	        this.keysToLeave.push(_key);
	      }
	    }

	    // If we want to someday check for reordering, we could do it here.
	  };

	  TransitionGroup.prototype.componentDidUpdate = function componentDidUpdate() {
	    var _this2 = this;

	    var keysToEnter = this.keysToEnter;
	    this.keysToEnter = [];
	    keysToEnter.forEach(function (key) {
	      return _this2.performEnter(key, _this2.childRefs[key]);
	    });

	    var keysToLeave = this.keysToLeave;
	    this.keysToLeave = [];
	    keysToLeave.forEach(function (key) {
	      return _this2.performLeave(key, _this2.childRefs[key]);
	    });
	  };

	  TransitionGroup.prototype.render = function render() {
	    var _this3 = this;

	    // TODO: we could get rid of the need for the wrapper node
	    // by cloning a single child
	    var childrenToRender = [];

	    var _loop = function _loop(key) {
	      var child = _this3.state.children[key];
	      if (child) {
	        var isCallbackRef = typeof child.ref !== 'string';
	        var factoryChild = _this3.props.childFactory(child);
	        var ref = function ref(r) {
	          _this3.childRefs[key] = r;
	        };

	        process.env.NODE_ENV !== 'production' ? (0, _warning2.default)(isCallbackRef, 'string refs are not supported on children of TransitionGroup and will be ignored. ' + 'Please use a callback ref instead: https://facebook.github.io/react/docs/refs-and-the-dom.html#the-ref-callback-attribute') : void 0;

	        // Always chaining the refs leads to problems when the childFactory
	        // wraps the child. The child ref callback gets called twice with the
	        // wrapper and the child. So we only need to chain the ref if the
	        // factoryChild is not different from child.
	        if (factoryChild === child && isCallbackRef) {
	          ref = (0, _chainFunction2.default)(child.ref, ref);
	        }

	        // You may need to apply reactive updates to a child as it is leaving.
	        // The normal React way to do it won't work since the child will have
	        // already been removed. In case you need this behavior you can provide
	        // a childFactory function to wrap every child, even the ones that are
	        // leaving.
	        childrenToRender.push(_react2.default.cloneElement(factoryChild, {
	          key: key,
	          ref: ref
	        }));
	      }
	    };

	    for (var key in this.state.children) {
	      _loop(key);
	    }

	    // Do not forward TransitionGroup props to primitive DOM nodes
	    var props = _extends({}, this.props);
	    delete props.transitionLeave;
	    delete props.transitionName;
	    delete props.transitionAppear;
	    delete props.transitionEnter;
	    delete props.childFactory;
	    delete props.transitionLeaveTimeout;
	    delete props.transitionEnterTimeout;
	    delete props.transitionAppearTimeout;
	    delete props.component;

	    return _react2.default.createElement(this.props.component, props, childrenToRender);
	  };

	  return TransitionGroup;
	}(_react2.default.Component);

	TransitionGroup.displayName = 'TransitionGroup';


	TransitionGroup.propTypes = process.env.NODE_ENV !== "production" ? propTypes : {};
	TransitionGroup.defaultProps = defaultProps;

	exports.default = TransitionGroup;
	module.exports = exports['default'];
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 37 */
/***/ (function(module, exports) {

	
	module.exports = function chain(){
	  var len = arguments.length
	  var args = [];

	  for (var i = 0; i < len; i++)
	    args[i] = arguments[i]

	  args = args.filter(function(fn){ return fn != null })

	  if (args.length === 0) return undefined
	  if (args.length === 1) return args[0]

	  return args.reduce(function(current, next){
	    return function chainedFunction() {
	      current.apply(this, arguments);
	      next.apply(this, arguments);
	    };
	  })
	}


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright 2014-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 */

	'use strict';

	/**
	 * Similar to invariant but only logs a warning if the condition is not met.
	 * This can be used to log issues in development environments in critical
	 * paths. Removing the logging code for production environments will keep the
	 * same logic and follow the same code paths.
	 */

	var warning = function() {};

	if (process.env.NODE_ENV !== 'production') {
	  warning = function(condition, format, args) {
	    var len = arguments.length;
	    args = new Array(len > 2 ? len - 2 : 0);
	    for (var key = 2; key < len; key++) {
	      args[key - 2] = arguments[key];
	    }
	    if (format === undefined) {
	      throw new Error(
	        '`warning(condition, format, ...args)` requires a warning ' +
	        'message argument'
	      );
	    }

	    if (format.length < 10 || (/^[s\W]*$/).test(format)) {
	      throw new Error(
	        'The warning format should be able to uniquely identify this ' +
	        'warning. Please, use a more descriptive format than: ' + format
	      );
	    }

	    if (!condition) {
	      var argIndex = 0;
	      var message = 'Warning: ' +
	        format.replace(/%s/g, function() {
	          return args[argIndex++];
	        });
	      if (typeof console !== 'undefined') {
	        console.error(message);
	      }
	      try {
	        // This error was thrown as a convenience so that you can use this stack
	        // to find the callsite that caused this warning to fire.
	        throw new Error(message);
	      } catch(x) {}
	    }
	  };
	}

	module.exports = warning;

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	exports.__esModule = true;
	exports.getChildMapping = getChildMapping;
	exports.mergeChildMappings = mergeChildMappings;

	var _react = __webpack_require__(24);

	/**
	 * Given `this.props.children`, return an object mapping key to child.
	 *
	 * @param {*} children `this.props.children`
	 * @return {object} Mapping of key to child
	 */
	function getChildMapping(children) {
	  if (!children) {
	    return children;
	  }
	  var result = {};
	  _react.Children.map(children, function (child) {
	    return child;
	  }).forEach(function (child) {
	    result[child.key] = child;
	  });
	  return result;
	}

	/**
	 * When you're adding or removing children some may be added or removed in the
	 * same render pass. We want to show *both* since we want to simultaneously
	 * animate elements in and out. This function takes a previous set of keys
	 * and a new set of keys and merges them with its best guess of the correct
	 * ordering. In the future we may expose some of the utilities in
	 * ReactMultiChild to make this easy, but for now React itself does not
	 * directly have this concept of the union of prevChildren and nextChildren
	 * so we implement it here.
	 *
	 * @param {object} prev prev children as returned from
	 * `ReactTransitionChildMapping.getChildMapping()`.
	 * @param {object} next next children as returned from
	 * `ReactTransitionChildMapping.getChildMapping()`.
	 * @return {object} a key set that contains all keys in `prev` and all keys
	 * in `next` in a reasonable order.
	 */
	function mergeChildMappings(prev, next) {
	  prev = prev || {};
	  next = next || {};

	  function getValueForKey(key) {
	    if (next.hasOwnProperty(key)) {
	      return next[key];
	    }

	    return prev[key];
	  }

	  // For each key of `next`, the list of keys to insert before that key in
	  // the combined list
	  var nextKeysPending = {};

	  var pendingKeys = [];
	  for (var prevKey in prev) {
	    if (next.hasOwnProperty(prevKey)) {
	      if (pendingKeys.length) {
	        nextKeysPending[prevKey] = pendingKeys;
	        pendingKeys = [];
	      }
	    } else {
	      pendingKeys.push(prevKey);
	    }
	  }

	  var i = void 0;
	  var childMapping = {};
	  for (var nextKey in next) {
	    if (nextKeysPending.hasOwnProperty(nextKey)) {
	      for (i = 0; i < nextKeysPending[nextKey].length; i++) {
	        var pendingNextKey = nextKeysPending[nextKey][i];
	        childMapping[nextKeysPending[nextKey][i]] = getValueForKey(pendingNextKey);
	      }
	    }
	    childMapping[nextKey] = getValueForKey(nextKey);
	  }

	  // Finally, add the keys which didn't appear before any key in `next`
	  for (i = 0; i < pendingKeys.length; i++) {
	    childMapping[pendingKeys[i]] = getValueForKey(pendingKeys[i]);
	  }

	  return childMapping;
	}

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	exports.__esModule = true;

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _addClass = __webpack_require__(41);

	var _addClass2 = _interopRequireDefault(_addClass);

	var _removeClass = __webpack_require__(44);

	var _removeClass2 = _interopRequireDefault(_removeClass);

	var _requestAnimationFrame = __webpack_require__(45);

	var _requestAnimationFrame2 = _interopRequireDefault(_requestAnimationFrame);

	var _properties = __webpack_require__(47);

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _propTypes = __webpack_require__(30);

	var _propTypes2 = _interopRequireDefault(_propTypes);

	var _reactDom = __webpack_require__(48);

	var _PropTypes = __webpack_require__(57);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var events = [];
	if (_properties.transitionEnd) events.push(_properties.transitionEnd);
	if (_properties.animationEnd) events.push(_properties.animationEnd);

	function addEndListener(node, listener) {
	  if (events.length) {
	    events.forEach(function (e) {
	      return node.addEventListener(e, listener, false);
	    });
	  } else {
	    setTimeout(listener, 0);
	  }

	  return function () {
	    if (!events.length) return;
	    events.forEach(function (e) {
	      return node.removeEventListener(e, listener, false);
	    });
	  };
	}

	var propTypes = {
	  children: _propTypes2.default.node,
	  name: _PropTypes.nameShape.isRequired,

	  // Once we require timeouts to be specified, we can remove the
	  // boolean flags (appear etc.) and just accept a number
	  // or a bool for the timeout flags (appearTimeout etc.)
	  appear: _propTypes2.default.bool,
	  enter: _propTypes2.default.bool,
	  leave: _propTypes2.default.bool,
	  appearTimeout: _propTypes2.default.number,
	  enterTimeout: _propTypes2.default.number,
	  leaveTimeout: _propTypes2.default.number
	};

	var CSSTransitionGroupChild = function (_React$Component) {
	  _inherits(CSSTransitionGroupChild, _React$Component);

	  function CSSTransitionGroupChild() {
	    var _temp, _this, _ret;

	    _classCallCheck(this, CSSTransitionGroupChild);

	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.componentWillAppear = function (done) {
	      if (_this.props.appear) {
	        _this.transition('appear', done, _this.props.appearTimeout);
	      } else {
	        done();
	      }
	    }, _this.componentWillEnter = function (done) {
	      if (_this.props.enter) {
	        _this.transition('enter', done, _this.props.enterTimeout);
	      } else {
	        done();
	      }
	    }, _this.componentWillLeave = function (done) {
	      if (_this.props.leave) {
	        _this.transition('leave', done, _this.props.leaveTimeout);
	      } else {
	        done();
	      }
	    }, _temp), _possibleConstructorReturn(_this, _ret);
	  }

	  CSSTransitionGroupChild.prototype.componentWillMount = function componentWillMount() {
	    this.classNameAndNodeQueue = [];
	    this.transitionTimeouts = [];
	  };

	  CSSTransitionGroupChild.prototype.componentWillUnmount = function componentWillUnmount() {
	    this.unmounted = true;

	    if (this.timeout) {
	      clearTimeout(this.timeout);
	    }
	    this.transitionTimeouts.forEach(function (timeout) {
	      clearTimeout(timeout);
	    });

	    this.classNameAndNodeQueue.length = 0;
	  };

	  CSSTransitionGroupChild.prototype.transition = function transition(animationType, finishCallback, timeout) {
	    var node = (0, _reactDom.findDOMNode)(this);

	    if (!node) {
	      if (finishCallback) {
	        finishCallback();
	      }
	      return;
	    }

	    var className = this.props.name[animationType] || this.props.name + '-' + animationType;
	    var activeClassName = this.props.name[animationType + 'Active'] || className + '-active';
	    var timer = null;
	    var removeListeners = void 0;

	    (0, _addClass2.default)(node, className);

	    // Need to do this to actually trigger a transition.
	    this.queueClassAndNode(activeClassName, node);

	    // Clean-up the animation after the specified delay
	    var finish = function finish(e) {
	      if (e && e.target !== node) {
	        return;
	      }

	      clearTimeout(timer);
	      if (removeListeners) removeListeners();

	      (0, _removeClass2.default)(node, className);
	      (0, _removeClass2.default)(node, activeClassName);

	      if (removeListeners) removeListeners();

	      // Usually this optional callback is used for informing an owner of
	      // a leave animation and telling it to remove the child.
	      if (finishCallback) {
	        finishCallback();
	      }
	    };

	    if (timeout) {
	      timer = setTimeout(finish, timeout);
	      this.transitionTimeouts.push(timer);
	    } else if (_properties.transitionEnd) {
	      removeListeners = addEndListener(node, finish);
	    }
	  };

	  CSSTransitionGroupChild.prototype.queueClassAndNode = function queueClassAndNode(className, node) {
	    var _this2 = this;

	    this.classNameAndNodeQueue.push({
	      className: className,
	      node: node
	    });

	    if (!this.rafHandle) {
	      this.rafHandle = (0, _requestAnimationFrame2.default)(function () {
	        return _this2.flushClassNameAndNodeQueue();
	      });
	    }
	  };

	  CSSTransitionGroupChild.prototype.flushClassNameAndNodeQueue = function flushClassNameAndNodeQueue() {
	    if (!this.unmounted) {
	      this.classNameAndNodeQueue.forEach(function (obj) {
	        // This is for to force a repaint,
	        // which is necessary in order to transition styles when adding a class name.
	        /* eslint-disable no-unused-expressions */
	        obj.node.scrollTop;
	        /* eslint-enable no-unused-expressions */
	        (0, _addClass2.default)(obj.node, obj.className);
	      });
	    }
	    this.classNameAndNodeQueue.length = 0;
	    this.rafHandle = null;
	  };

	  CSSTransitionGroupChild.prototype.render = function render() {
	    var props = _extends({}, this.props);
	    delete props.name;
	    delete props.appear;
	    delete props.enter;
	    delete props.leave;
	    delete props.appearTimeout;
	    delete props.enterTimeout;
	    delete props.leaveTimeout;
	    delete props.children;
	    return _react2.default.cloneElement(_react2.default.Children.only(this.props.children), props);
	  };

	  return CSSTransitionGroupChild;
	}(_react2.default.Component);

	CSSTransitionGroupChild.displayName = 'CSSTransitionGroupChild';


	CSSTransitionGroupChild.propTypes = process.env.NODE_ENV !== "production" ? propTypes : {};

	exports.default = CSSTransitionGroupChild;
	module.exports = exports['default'];
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)))

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var _interopRequireDefault = __webpack_require__(42);

	exports.__esModule = true;
	exports.default = addClass;

	var _hasClass = _interopRequireDefault(__webpack_require__(43));

	function addClass(element, className) {
	  if (element.classList) element.classList.add(className);else if (!(0, _hasClass.default)(element, className)) if (typeof element.className === 'string') element.className = element.className + ' ' + className;else element.setAttribute('class', (element.className && element.className.baseVal || '') + ' ' + className);
	}

	module.exports = exports["default"];

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : {
	    "default": obj
	  };
	}

	module.exports = _interopRequireDefault;

/***/ }),
/* 43 */
/***/ (function(module, exports) {

	"use strict";

	exports.__esModule = true;
	exports.default = hasClass;

	function hasClass(element, className) {
	  if (element.classList) return !!className && element.classList.contains(className);else return (" " + (element.className.baseVal || element.className) + " ").indexOf(" " + className + " ") !== -1;
	}

	module.exports = exports["default"];

/***/ }),
/* 44 */
/***/ (function(module, exports) {

	'use strict';

	function replaceClassName(origClass, classToRemove) {
	  return origClass.replace(new RegExp('(^|\\s)' + classToRemove + '(?:\\s|$)', 'g'), '$1').replace(/\s+/g, ' ').replace(/^\s*|\s*$/g, '');
	}

	module.exports = function removeClass(element, className) {
	  if (element.classList) element.classList.remove(className);else if (typeof element.className === 'string') element.className = replaceClassName(element.className, className);else element.setAttribute('class', replaceClassName(element.className && element.className.baseVal || '', className));
	};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var _interopRequireDefault = __webpack_require__(42);

	exports.__esModule = true;
	exports.default = void 0;

	var _inDOM = _interopRequireDefault(__webpack_require__(46));

	var vendors = ['', 'webkit', 'moz', 'o', 'ms'];
	var cancel = 'clearTimeout';
	var raf = fallback;
	var compatRaf;

	var getKey = function getKey(vendor, k) {
	  return vendor + (!vendor ? k : k[0].toUpperCase() + k.substr(1)) + 'AnimationFrame';
	};

	if (_inDOM.default) {
	  vendors.some(function (vendor) {
	    var rafKey = getKey(vendor, 'request');

	    if (rafKey in window) {
	      cancel = getKey(vendor, 'cancel');
	      return raf = function raf(cb) {
	        return window[rafKey](cb);
	      };
	    }
	  });
	}
	/* https://github.com/component/raf */


	var prev = new Date().getTime();

	function fallback(fn) {
	  var curr = new Date().getTime(),
	      ms = Math.max(0, 16 - (curr - prev)),
	      req = setTimeout(fn, ms);
	  prev = curr;
	  return req;
	}

	compatRaf = function compatRaf(cb) {
	  return raf(cb);
	};

	compatRaf.cancel = function (id) {
	  window[cancel] && typeof window[cancel] === 'function' && window[cancel](id);
	};

	var _default = compatRaf;
	exports.default = _default;
	module.exports = exports["default"];

/***/ }),
/* 46 */
/***/ (function(module, exports) {

	"use strict";

	exports.__esModule = true;
	exports.default = void 0;

	var _default = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

	exports.default = _default;
	module.exports = exports["default"];

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var _interopRequireDefault = __webpack_require__(42);

	exports.__esModule = true;
	exports.default = exports.animationEnd = exports.animationDelay = exports.animationTiming = exports.animationDuration = exports.animationName = exports.transitionEnd = exports.transitionDuration = exports.transitionDelay = exports.transitionTiming = exports.transitionProperty = exports.transform = void 0;

	var _inDOM = _interopRequireDefault(__webpack_require__(46));

	var transform = 'transform';
	exports.transform = transform;
	var prefix, transitionEnd, animationEnd;
	exports.animationEnd = animationEnd;
	exports.transitionEnd = transitionEnd;
	var transitionProperty, transitionDuration, transitionTiming, transitionDelay;
	exports.transitionDelay = transitionDelay;
	exports.transitionTiming = transitionTiming;
	exports.transitionDuration = transitionDuration;
	exports.transitionProperty = transitionProperty;
	var animationName, animationDuration, animationTiming, animationDelay;
	exports.animationDelay = animationDelay;
	exports.animationTiming = animationTiming;
	exports.animationDuration = animationDuration;
	exports.animationName = animationName;

	if (_inDOM.default) {
	  var _getTransitionPropert = getTransitionProperties();

	  prefix = _getTransitionPropert.prefix;
	  exports.transitionEnd = transitionEnd = _getTransitionPropert.transitionEnd;
	  exports.animationEnd = animationEnd = _getTransitionPropert.animationEnd;
	  exports.transform = transform = prefix + "-" + transform;
	  exports.transitionProperty = transitionProperty = prefix + "-transition-property";
	  exports.transitionDuration = transitionDuration = prefix + "-transition-duration";
	  exports.transitionDelay = transitionDelay = prefix + "-transition-delay";
	  exports.transitionTiming = transitionTiming = prefix + "-transition-timing-function";
	  exports.animationName = animationName = prefix + "-animation-name";
	  exports.animationDuration = animationDuration = prefix + "-animation-duration";
	  exports.animationTiming = animationTiming = prefix + "-animation-delay";
	  exports.animationDelay = animationDelay = prefix + "-animation-timing-function";
	}

	var _default = {
	  transform: transform,
	  end: transitionEnd,
	  property: transitionProperty,
	  timing: transitionTiming,
	  delay: transitionDelay,
	  duration: transitionDuration
	};
	exports.default = _default;

	function getTransitionProperties() {
	  var style = document.createElement('div').style;
	  var vendorMap = {
	    O: function O(e) {
	      return "o" + e.toLowerCase();
	    },
	    Moz: function Moz(e) {
	      return e.toLowerCase();
	    },
	    Webkit: function Webkit(e) {
	      return "webkit" + e;
	    },
	    ms: function ms(e) {
	      return "MS" + e;
	    }
	  };
	  var vendors = Object.keys(vendorMap);
	  var transitionEnd, animationEnd;
	  var prefix = '';

	  for (var i = 0; i < vendors.length; i++) {
	    var vendor = vendors[i];

	    if (vendor + "TransitionProperty" in style) {
	      prefix = "-" + vendor.toLowerCase();
	      transitionEnd = vendorMap[vendor]('TransitionEnd');
	      animationEnd = vendorMap[vendor]('AnimationEnd');
	      break;
	    }
	  }

	  if (!transitionEnd && 'transitionProperty' in style) transitionEnd = 'transitionend';
	  if (!animationEnd && 'animationName' in style) animationEnd = 'animationend';
	  style = null;
	  return {
	    animationEnd: animationEnd,
	    transitionEnd: transitionEnd,
	    prefix: prefix
	  };
	}

/***/ }),
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	exports.__esModule = true;
	exports.nameShape = undefined;
	exports.transitionTimeout = transitionTimeout;

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	var _propTypes = __webpack_require__(30);

	var _propTypes2 = _interopRequireDefault(_propTypes);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function transitionTimeout(transitionType) {
	  var timeoutPropName = 'transition' + transitionType + 'Timeout';
	  var enabledPropName = 'transition' + transitionType;

	  return function (props) {
	    // If the transition is enabled
	    if (props[enabledPropName]) {
	      // If no timeout duration is provided
	      if (props[timeoutPropName] == null) {
	        return new Error(timeoutPropName + ' wasn\'t supplied to CSSTransitionGroup: ' + 'this can cause unreliable animations and won\'t be supported in ' + 'a future version of React. See ' + 'https://fb.me/react-animation-transition-group-timeout for more ' + 'information.');

	        // If the duration isn't a number
	      } else if (typeof props[timeoutPropName] !== 'number') {
	        return new Error(timeoutPropName + ' must be a number (in milliseconds)');
	      }
	    }

	    return null;
	  };
	}

	var nameShape = exports.nameShape = _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.shape({
	  enter: _propTypes2.default.string,
	  leave: _propTypes2.default.string,
	  active: _propTypes2.default.string
	}), _propTypes2.default.shape({
	  enter: _propTypes2.default.string,
	  enterActive: _propTypes2.default.string,
	  leave: _propTypes2.default.string,
	  leaveActive: _propTypes2.default.string,
	  appear: _propTypes2.default.string,
	  appearActive: _propTypes2.default.string
	})]);

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(24);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var outs = {
		"170": ["T20", "T20", "DB"],
		"167": ["T20", "T19", "DB"],
		"164": ["T20", "T18", "DB"],
		"161": ["T20", "T17", "DB"],
		"160": ["T20", "T20", "D20"],
		"158": ["T20", "T20", "D19"],
		"157": ["T20", "T19", "D20"],
		"156": ["T20", "T20", "D18"],
		"155": ["T20", "T19", "D19"],
		"154": ["T20", "T18", "D20"],
		"153": ["T20", "T19", "D18"],
		"152": ["T20", "T20", "D16"],
		"151": ["T20", "T17", "D20"],
		"150": ["T20", "T18", "D18"],
		"149": ["T20", "T19", "D16"],
		"148": ["T20", "T20", "D14"],
		"147": ["T20", "T17", "D18"],
		"146": ["T20", "T18", "D16"],
		"145": ["T20", "T19", "D14"],
		"144": ["T20", "T20", "D12"],
		"143": ["T20", "T17", "D16"],
		"142": ["T20", "DB", "D16"],
		"141": ["T19", "T16", "D18"],
		"140": ["T20", "T20", "D10"],
		"139": ["T20", "T13", "D20"],
		"138": ["T20", "T18", "D12"],
		"137": ["T20", "T15", "D16"],
		"136": ["T20", "T20", "D8"],
		"135": ["S25", "T20", "DB"],
		"134": ["T20", "T14", "D16"],
		"133": ["T19", "T20", "D8"],
		"132": ["25", "T20", "DB"],
		"131": ["T20", "T13", "D16"],
		"130": ["T20", "T20", "D5"],
		"129": ["T19", "T12", "D18"],
		"128": ["T18", "T14", "D16"],
		"127": ["T20", "T17", "D8"],
		"126": ["T19", "T19", "D6"],
		"125": ["T18", "T13", "D16"],
		"124": ["T20", "T16", "D8"],
		"123": ["T19", "T16", "D9"],
		"122": ["T18", "T20", "D4"],
		"121": ["T17", "T18", "D8"],
		"120": ["T20", "S20", "D20"],
		"119": ["T19", "T12", "D13"],
		"118": ["T20", "S18", "D20"],
		"117": ["T20", "S17", "D20"],
		"116": ["T20", "S16", "D20"],
		"115": ["T19", "S18", "D20"],
		"114": ["T20", "S14", "D20"],
		"113": ["T19", "S16", "D20"],
		"112": ["T20", "T12", "D8"],
		"111": ["T20", "S19", "D16"],
		"110": ["T20", "DB"],
		"109": ["T20", "S17", "D16"],
		"108": ["T20", "S16", "D16"],
		"107": ["T19", "DB"],
		"106": ["T20", "S6", "D20"],
		"105": ["T20", "S13", "D16"],
		"104": ["T18", "DB"],
		"103": ["T19", "S6", "D20"],
		"102": ["T20", "S10", "D16"],
		"101": ["T20", "S9", "D16"],
		"100": ["T20", "D20"],
		"99": ["T19", "S10", "D16"],
		"98": ["T20", "D19"],
		"97": ["T19", "D20"],
		"96": ["T20", "D18"],
		"95": ["T19", "D19"],
		"94": ["T18", "D20"],
		"93": ["T19", "D18"],
		"92": ["T20", "D16"],
		"91": ["T17", "D20"],
		"90": ["T20", "D15"],
		"89": ["T19", "D16"],
		"88": ["T20", "D14"],
		"87": ["T17", "D18"],
		"86": ["T18", "D16"],
		"85": ["T19", "D14"],
		"84": ["T20", "D12"],
		"83": ["T17", "D16"],
		"82": ["T14", "D20"],
		"81": ["T19", "D12"],
		"80": ["T20", "D10"],
		"79": ["T13", "D20"],
		"78": ["T18", "D12"],
		"77": ["T19", "D10"],
		"76": ["T20", "D8"],
		"75": ["T17", "D12"],
		"74": ["T14", "D16"],
		"73": ["T19", "D8"],
		"72": ["T12", "D18"],
		"71": ["T13", "D16"],
		"70": ["T18", "D8"],
		"69": ["T19", "D6"],
		"68": ["T20", "D4"],
		"67": ["T17", "D8"],
		"66": ["T10", "D18"],
		"65": ["SB ", "D20"],
		"64": ["T16", "D8"],
		"63": ["T17", "D6"],
		"62": ["T10", "D16"],
		"61": ["T15", "D8"],
		"60": ["S20", "D20"],
		"59": ["S19", "D20"],
		"58": ["S18", "D20"],
		"57": ["S17", "D20"],
		"56": ["T16", "D4"],
		"55": ["S15", "D20"],
		"54": ["S14", "D20"],
		"53": ["S17", "D18"],
		"52": ["T12", "D8"],
		"51": ["S19", "D16"],
		"50": ["DB"],
		"49": ["S17", "D16"],
		"48": ["S16", "D16"],
		"47": ["S15", "D16"],
		"46": ["S6", "D20"],
		"45": ["S13", "D16"],
		"44": ["S12", "D16"],
		"43": ["S11", "D16"],
		"42": ["S10", "D16"],
		"41": ["S9", "D16"],

		"40": ["D20"],
		"39": ["S7 D16"],
		"38": ["D19"],
		"37": ["S5 D16"],
		"36": ["D18"],
		"35": ["S3 D16"],
		"34": ["D17"],
		"33": ["S1 D16"],
		"32": ["D16"],
		"31": ["S7 D12"],
		"30": ["D15"],
		"29": ["S1 D14"],
		"28": ["D14"],
		"27": ["S3 D12"],
		"26": ["D13"],
		"25": ["S1 D12"],
		"24": ["D12"],
		"23": ["S7 D8"],
		"22": ["D11"],
		"21": ["S5 D8"],
		"20": ["D10"],
		"19": ["S3 D8"],
		"18": ["D9"],
		"17": ["S1 D8"],
		"16": ["D8"],
		"15": ["S3 D6"],
		"14": ["D7"],
		"13": ["S1 D6"],
		"12": ["D6"],
		"11": ["S3 D4"],
		"10": ["D5"],
		"9": ["S1 D4"],
		"8": ["D4"],
		"7": ["S3 D2"],
		"6": ["D3"],
		"5": ["S1 D2"],
		"4": ["D2"],
		"3": ["S1 D1"],
		"2": ["D1"],
		"1": [""]
	};

	var OutItem = function OutItem(props) {
		return _react2.default.createElement(
			"li",
			{ className: "out-item" },
			_react2.default.createElement(
				"h1",
				null,
				props.out
			)
		);
	};

	var Outchart = function (_React$Component) {
		_inherits(Outchart, _React$Component);

		function Outchart() {
			_classCallCheck(this, Outchart);

			return _possibleConstructorReturn(this, (Outchart.__proto__ || Object.getPrototypeOf(Outchart)).apply(this, arguments));
		}

		_createClass(Outchart, [{
			key: "renderOuts",
			value: function renderOuts() {
				var _this2 = this;

				var currentScore = this.props.currentScore;
				var shouldCountArrows = Object.keys(this.props.arrows).length > 0;
				var tempScore = void 0;

				if (shouldCountArrows) {
					var thrownScore = Object.keys(this.props.arrows).filter(function (key) {
						return _this2.props.arrows[key].player == _this2.props.player;
					}).reduce(function (start, next) {
						return start + _this2.props.arrows[next].score;
					}, 0);

					console.log("Thrown score: ", thrownScore);
					tempScore = currentScore - thrownScore;
				} else {
					tempScore = currentScore;
				}

				if (outs[tempScore] && outs[tempScore][0]) {
					return _react2.default.createElement(
						"ul",
						{ className: "out-list" },
						outs[tempScore].map(function (data, idx) {
							return _react2.default.createElement(OutItem, { out: data, key: idx });
						})
					);
				}

				return _react2.default.createElement(
					"div",
					{ className: "out-list" },
					_react2.default.createElement(
						"h1",
						{ className: "no-finish" },
						"NO FINISH"
					)
				);
			}
		}, {
			key: "render",
			value: function render() {
				return _react2.default.createElement(
					"div",
					{ className: "outchart" },
					this.renderOuts()
				);
			}
		}]);

		return Outchart;
	}(_react2.default.Component);

	exports.default = Outchart;

/***/ })
]);