import connection from "./../../components/connection";
import { CSSTransitionGroup } from "react-transition-group";
import Outchart from "./outchart.js";
import React from "react";

const top_score = 301;

const Score = props => {
	return (
		<div className="player-score">
			<h1>{props.score}</h1>
		</div>
	);
};

const Arrow = props => {
	if (props.player != props.data.player) return null;

	return (
		<CSSTransitionGroup
			transitionName="fly"
			transitionEnterTimeout={200}
			transitionLeaveTimeout={200}
			transitionAppear={true}
			transitionAppearTimeout={1000}
		>
			<div className={"arrow"} key={"arrow-" + props.idx}>
				<img src="./app/images/dart-arrow.png" />
				<span>{props.data.score}</span>
			</div>
		</CSSTransitionGroup>
	);
};

const TripleArrow = props => {
	return (
		<CSSTransitionGroup
			transitionName="triple"
			transitionEnterTimeout={200}
			transitionLeaveTimeout={200}
			transitionAppear={true}
			transitionAppearTimeout={700}
		>
			<div className="arrows triple-arrow" key={"triple-arrow"}>
				<div className="arrow">
					<img src="./app/images/dart-arrow.png" />
					<span>{props.arrows[0].score}</span>
				</div>
				<div className="arrow fly">
					<img src="./app/images/triple-dart-arrow.png" />
					<span>{props.totalScore}</span>
				</div>
				<div className="arrow">
					<img src="./app/images/dart-arrow.png" />
					<span>{props.arrows[2].score}</span>
				</div>
			</div>
		</CSSTransitionGroup>
	);
};

const Arrows = props => {
	const arrowdata = Object.keys(props.data).map(key => props.data[key]);

	if (arrowdata.length == 3 && props.player == arrowdata[0].player) {
		const totalScore = arrowdata.reduce(
			(start, next) => start + next.score,
			0
		);
		return <TripleArrow totalScore={totalScore} arrows={arrowdata} />;
	}

	return (
		<div className="arrows">
			{arrowdata.map((arrow, idx) => (
				<Arrow data={arrow} player={props.player} key={idx} idx={idx} />
			))}
		</div>
	);
};

const ScoreAnimation = props => {
	if (!props.show) return null;

	const randomAnimation = Math.random() * 100000;

	return (
		<div className="score-animation">
			<img src={`./app/images/${props.show}.gif?v=${randomAnimation}`} />
		</div>
	);
};

const VictoryOverlay = props => {
	if (!props.show.active) return null;

	const random = Math.random() * 10000;
	const TOTAL_GIFS = 8;
	const randomGif = Math.floor(Math.random() * TOTAL_GIFS);

	return (
		<div
			className="victory-overlay"
			style={{
				backgroundImage: `url('./app/images/players-agency/${props.show.player}Win.jpg')`
			}}
		></div>
	);
};

export default class Scoreboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			settings: {
				playerA: { name: "...", score: top_score, legs: 0 },
				playerB: { name: "...", score: top_score, legs: 0 },
				isPlayerAsTurn: true,
				legs: 3,
				type: "...",
				legStarter: "playerA"
			},
			arrows: {},
			victory: { active: false, player: null }
		};

		this.db = connection.fb().database();
	}

	componentDidMount() {
		this.db
			.ref("battle/settings")
			.on("value", snapshot =>
				this.setState({ settings: snapshot.val() })
			);
		this.db
			.ref("battle/victory")
			.on("value", snapshot =>
				this.setState({ victory: snapshot.val() })
			);
		this.db.ref("battle/arrows").on("value", snapshot => {
			const arrows = snapshot.val() !== null ? snapshot.val() : {};
			this.setState({ arrows });
		});
	}

	getNewScore(currentScore, arrowValue) {
		return currentScore - arrowValue;
	}

	getPlayersCurrentScore(player) {
		return player.score;
	}

	getCurrentPlayersTurn() {
		if (this.state.settings.isPlayerAsTurn) {
			return this.state.settings.playerA;
		}

		return this.state.settings.playerB;
	}

	getBestOf() {
		return <span>{this.props.bestOf}</span>;
	}

	checkEndMatch() {
		var totalLegs =
				this.state.settings.playerA.legs +
				this.state.settings.playerB.legs,
			bestOf = this.props.bestOf,
			minimumLegsToWin = (parseInt(bestOf) + 1) * 0.5;

		if (
			(this.state.settings.playerA.legs >= minimumLegsToWin ||
				this.state.settings.playerB.legs >= minimumLegsToWin) &&
			totalLegs >= minimumLegsToWin
		) {
			this.props.onEndGame(this.getCurrentPlayersTurn().name);
		}
	}

	renderOutchart() {
		var currentPlayer = this.getCurrentPlayersTurn();

		return (
			<Outchart
				currentScore={this.getPlayersCurrentScore(currentPlayer)}
				currentPlayer={currentPlayer}
			/>
		);
	}

	renderRedDot(player) {
		if (player == this.getCurrentPlayersTurn()) {
			return <div className="current-player"></div>;
		}

		return null;
	}

	renderFooter() {
		var stage;

		if (this.props.bestOf == 5) {
			stage = "- SEMI FINAL";
		}

		if (this.props.bestOf == 7) {
			stage = "- FINAL";
		}

		return (
			<div className="scoreboard-footer">
				<h1>
					FX Open 2019 -{" "}
					<span className="battleType">
						{this.state.settings.type}
					</span>
				</h1>
			</div>
		);
	}

	isInt(value) {
		return (
			!isNaN(value) &&
			(function(x) {
				return (x | 0) === x;
			})(parseFloat(value))
		);
	}

	handleGoBack() {
		this.props.onEndGame("noone");
	}

	pickAnimation(tempScore, thrownScore) {
		if (tempScore == 0) {
			return "finish";
		} else if (tempScore < 1) {
			return "death";
		}
		return false;
	}

	isPlayerFinished(player) {
		const currentPlayer = this.state.settings.isPlayerAsTurn
			? "playerA"
			: "playerB";
		if (currentPlayer != player) return false;

		const currentScore = this.state.settings[player].score;
		const shouldCountArrows = Object.keys(this.state.arrows).length > 0;
		let tempScore;
		let thrownScore;

		if (shouldCountArrows) {
			thrownScore = Object.keys(this.state.arrows)
				.filter(key => this.state.arrows[key].player == currentPlayer)
				.reduce(
					(start, next) => start + this.state.arrows[next].score,
					0
				);

			tempScore = currentScore - thrownScore;
		} else {
			tempScore = currentScore;
		}
		return this.pickAnimation(tempScore, thrownScore);
	}

	render() {
		return (
			<div className="container-scoreboard">
				<div className="scoreboard-header">
					<div className="scoreboard-header--left">
						<h1>
							Best of {this.state.settings.legs}{" "}
							{this.state.settings.legs == 1 ? "leg" : "legs"}
						</h1>
					</div>
					<div className="scoreboard-header--right">
						<h1>Legs</h1>
					</div>
					<div className="scoreboard-header--right">
						<h1>Pts</h1>
					</div>
				</div>
				<div className="scoreboard-content">
					<VictoryOverlay show={this.state.victory} />
					<div className="player-row player-row--a">
						<div className="player-name">
							<h1>{this.state.settings.playerA.name}</h1>
							{this.renderRedDot(this.state.settings.playerA)}
						</div>
						<Arrows data={this.state.arrows} player={"playerA"} />
						<div
							className={
								this.state.settings.legStarter == "playerA"
									? "player-stats starter"
									: "player-stats"
							}
						>
							<div className="player-legs">
								<h1>{this.state.settings.playerA.legs}</h1>
							</div>
							<Score score={this.state.settings.playerA.score} />
							<Outchart
								player={"playerA"}
								currentScore={this.state.settings.playerA.score}
								arrows={
									this.state.settings.isPlayerAsTurn
										? this.state.arrows
										: {}
								}
							/>
							<ScoreAnimation
								show={this.isPlayerFinished("playerA")}
							/>
						</div>
					</div>
					<div className="player-row player-row--b">
						<div className="player-name">
							<h1
								className={
									this.state.settings.legStarter == "playerB"
										? "underline"
										: ""
								}
							>
								{this.state.settings.playerB.name}
							</h1>
							{this.renderRedDot(this.state.settings.playerB)}
						</div>
						<Arrows data={this.state.arrows} player={"playerB"} />
						<div
							className={
								this.state.settings.legStarter == "playerB"
									? "player-stats starter"
									: "player-stats"
							}
						>
							<div className="player-legs">
								<h1>{this.state.settings.playerB.legs}</h1>
							</div>
							<Score score={this.state.settings.playerB.score} />
							<Outchart
								player={"playerB"}
								currentScore={this.state.settings.playerB.score}
								arrows={
									this.state.settings.isPlayerAsTurn
										? {}
										: this.state.arrows
								}
							/>
							<ScoreAnimation
								show={this.isPlayerFinished("playerB")}
							/>
						</div>
					</div>
				</div>
				{this.renderFooter()}
			</div>
		);
	}
}
