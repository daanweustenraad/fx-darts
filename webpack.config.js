var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    entry: {
      scoreboard: "./app/js/main.js",
      server: "./server.js"
    },
    output: {
        path: __dirname + '/app/js',
        filename: "[name].bundle.js",
        publicPath: '/',
        chunkFilename: '[id].bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
            { test: /\.css$/, loader: "style!css" },
        ]
    },
    plugins: [
      new CommonsChunkPlugin({
        filename: 'commons.js',
        name: 'commons'
      })
    ]
};
